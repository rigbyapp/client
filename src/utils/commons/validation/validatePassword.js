/**
 * Validates if the password hast the minimum safety requierements
 * @param {*} password The password to be validated
 * @return {Boolean} true/false
 */
export function validatePassword(password) {
  const REGEX_VALIDATION = require('@rigby/data/REGEX_VALIDATION.json');
  const passFormat = REGEX_VALIDATION.PASSWORD;
  if (password.match(passFormat)) {
    return true;
  }
  return false;
}
