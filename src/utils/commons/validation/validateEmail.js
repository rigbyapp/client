/**
 * Validates if the string is an email
 * @param {String} email The mail to be validate
 * @return {Boolean} true/false
 */
export function validateEmail(email) {
  const REGEX_VALIDATION = require('@rigby/data/REGEX_VALIDATION.json');
  const mailFormat = REGEX_VALIDATION.E_MAIL;
  if (email.match(mailFormat)) {
    return true;
  }
  return false;
}
