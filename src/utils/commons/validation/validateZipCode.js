/**
 * Validates a zipcode given a country code
 * @param {String} zipcode The zipcode to be validated
 * @param {String} countryCode The country code the zip code belongs to
 * @return {Boolean} true/false
 */
export function validateZipcode(zipcode, countryCode) {
  const ZIP_CODE_DATA = require('@rigby/data/REGEX_VALIDATION.json');
  const zipCodeByCountry = ZIP_CODE_DATA.ZIP_CODE[countryCode];
  if (zipcode.match(zipCodeByCountry)) {
    return true;
  }
  return false;
}
