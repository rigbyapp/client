/**
 * Validates if is a name
 * @param {String} name The name to be validated
 * @return {Boolean} true/false
 */
export function validateName(name) {
  const REGEX_VALIDATION = require('@rigby/data/REGEX_VALIDATION.json');
  const nameFormat = REGEX_VALIDATION.NAME;
  if (name.match(nameFormat)) {
    return true;
  }
  return false;
}
