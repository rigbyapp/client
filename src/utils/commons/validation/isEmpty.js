const REGEX_VALIDATION = require('@rigby/data/REGEX_VALIDATION.json');

/**
 * Validates if a string is empty
 * @param {String} string The string to be validated
 * @return {Boolean} true/false
 */
export function isEmpty(string = '') {
  const emptyFormat = REGEX_VALIDATION.EMPTY;
  if (string.match(emptyFormat)) {
    return true;
  }
  return false;
}
/**
 *  Validates if a string is not empty
 * @param {String} string The string to be validated
 * @return {Boolean} true/false
 */
export function isFilled(string = '') {
  const emptyFormat = REGEX_VALIDATION.EMPTY;
  if (string.match(emptyFormat)) {
    return false;
  }
  return true;
}
