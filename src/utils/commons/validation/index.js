import {validateEmail} from './validateEmail';
import {validatePassword} from './validatePassword';
import {isEmpty, isFilled} from './isEmpty';
import {validateName} from './validateName';
import {validateZipcode} from './validateZipCode';

export const validation = {
  email: validateEmail,
  password: validatePassword,
  name: validateName,
  zipCode: validateZipcode,
  empty: isEmpty,
  filled: isFilled,
  none: () => {
    return true;
  },
};
