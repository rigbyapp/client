import React from 'react';
import {PostDetail} from '@rigby/components/layout/PostDetail';
/**
 * Redirects to the general app modal with an post component inside
 * @param {number} postId The id of the post
 * @param {object} appConfig the general app config context
 * @param {function fecthProfileData()} fetchProfileData for fetching the profile data
 */
export function redirectToPost(post, appConfig, fetchProfileData) {
  appConfig.setGeneralModal(true);
  appConfig.setGeneralModalComponent(
    <PostDetail post={post} fetchProfileData={fetchProfileData} />,
  );
}
