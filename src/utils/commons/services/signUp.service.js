import {ENV_CONFIG} from '@rigby/config';

const axios = require('axios');
const jwt_decode = require('jwt-decode');
/**
 * Makes an API call to the sign up service
 * @param {String} email The email to be registered
 * @param {String} password The password to be registered
 * @param {String} firstName The user's first name
 * @param {String} lastName The user's last name
 * @param {String} zipCode The user's zipcode
 * @param {String} birthDate The user's birthday in ISO 8601 format
 * @return A JWT token and a profile picture.
 */
export async function signUpService(
  email,
  password,
  firstName,
  lastName,
  zipCode,
  birthDate,
) {
  const url = `${ENV_CONFIG.HOST_BACKEND}users/register`;
  const axiosConfig = {
    method: 'post',
    url: url,
    data: {
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password,
      birthDate: birthDate,
      zipcode: zipCode,
    },
  };
  try {
    const res = await axios(axiosConfig);
    const response = {
      token: `Bearer ${res.data.token}`,
      data: {
        ...jwt_decode(res.data.token),
        profilePicture: res.data.profilePicture,
      },
      auth: true,
    };
    //response.append(res.data.profilePicture);
    return response;
  } catch (error) {
    const response = {data: error.response.data.message, auth: false};
    console.log(`Error: ${error.response.data.message}`);
    return response;
  }
}
