import {ENV_CONFIG} from '@rigby/config';
const axios = require('axios');
/**
 * Makes an API call for the post of a given user
 * @param {String} authToken The token of the current user
 * @param {Number} userId The id of the user we are fetching
 * @return An array of posts
 */
export async function fetchUserFeed(authToken, userId) {
  const url = `${ENV_CONFIG.HOST_BACKEND}post/by_user/${userId}`;
  const axiosConfig = {
    method: 'get',
    url: url,
    headers: {
      Authorization: authToken,
    },
  };
  try {
    const res = await axios(axiosConfig);
    const response = {
      data: res.data,
    };
    //console.log(response.data);
    return response;
  } catch (error) {
    const response = {data: error.response.data.message};
    console.log(`Error: ${error.response.data.message}`);
    return response;
  }
}
