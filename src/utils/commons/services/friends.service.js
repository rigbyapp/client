import {ENV_CONFIG} from '@rigby/config';
const axios = require('axios');
/**
 * Makes an API call to the friends endpoint
 * @param {String} authToken The token of the current user
 * @return {Promise} An array of the user's friends
 */
export async function fetchFriends(authToken) {
  const url = `${ENV_CONFIG.HOST_BACKEND}friends`;
  const axiosConfig = {
    method: 'get',
    url: url,
    headers: {
      Authorization: authToken,
    },
  };
  try {
    const res = await axios(axiosConfig);
    const response = {
      data: res.data,
    };
    console.log(response.data);
    return response;
  } catch (error) {
    const response = {data: error.response.data.message};
    console.log(`Error: ${error.response.data.message}`);
    return response;
  }
}
