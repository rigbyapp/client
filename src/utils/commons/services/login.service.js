import {ENV_CONFIG} from '@rigby/config';

const axios = require('axios');
const jwt_decode = require('jwt-decode');
/**
 * Makes an API call to the login service
 * @param {String} email The user's email
 * @param {String} password The user's password
 * @return A JWT token and a profile picture.
 */
export async function loginService(email, password) {
  const url = `${ENV_CONFIG.HOST_BACKEND}users/login`;
  const axiosConfig = {
    method: 'post',
    url: url,
    data: {
      email: email,
      password: password,
    },
  };
  try {
    const res = await axios(axiosConfig);
    const response = {
      token: `Bearer ${res.data.token}`,
      data: {
        ...jwt_decode(res.data.token),
        profilePicture: res.data.profilePicture,
      },
      auth: true,
    };
    console.log(response.data);
    return response;
  } catch (error) {
    const response = {data: error.response.data.message, auth: false};
    console.log(`Error: ${error.response.data.message}`);
    return response;
  }
}
