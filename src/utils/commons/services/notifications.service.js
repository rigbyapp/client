import {ENV_CONFIG} from '@rigby/config';
const axios = require('axios');

/**
 * Fetches an user's notifications
 * @param {*} authToken
 * @returns An array of notificaitons
 */
export async function fetchNotifications(authToken) {
  const url = `${ENV_CONFIG.HOST_BACKEND}notifications`;
  const axiosConfig = {
    method: 'get',
    url: url,
    headers: {
      Authorization: authToken,
    },
  };
  try {
    const res = await axios(axiosConfig);
    const response = {
      data: res.data,
    };
    console.log(response.data);
    return response;
  } catch (error) {
    const response = {data: error.response.data.message};
    console.log(`Error: ${error.response.data.message}`);
    return response;
  }
}
