import {ENV_CONFIG} from '@rigby/config';
const axios = require('axios');
/**
 * Makes an API call to the friend suggestiongs endpoint
 * @param {String} authToken The token of the current user
 * @return An array of users
 */
export async function fetchFriendSuggestions(authToken) {
  const url = `${ENV_CONFIG.HOST_BACKEND}users/suggestions`;
  const axiosConfig = {
    method: 'get',
    url: url,
    headers: {
      Authorization: authToken,
    },
  };
  try {
    const res = await axios(axiosConfig);
    const response = {
      data: res.data,
    };
    //console.log(response.data);
    return response;
  } catch (error) {
    const response = {data: error.response.data.message};
    console.log(`Error on service: ${error.response.data.message}`);
    return response;
  }
}
