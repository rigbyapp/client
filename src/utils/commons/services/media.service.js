import {ENV_CONFIG} from '@rigby/config';

const axios = require('axios');
export async function fetchMediaFile(authToken) {
  const url = `${ENV_CONFIG.HOST_BACKEND}/1belleza.jpg`;
  const axiosConfig = {
    method: 'get',
    url: url,
    headers: {
      Authorization: authToken,
    },
  };
  try {
    const res = await axios(axiosConfig);
    const response = {
      data: res.data,
    };
    //console.log(response.data);
    return response;
  } catch (error) {
    const response = {data: error.response.data.message, auth: false};
    console.log(`Error: ${error.response.data.message}`);
    return response;
  }
}
