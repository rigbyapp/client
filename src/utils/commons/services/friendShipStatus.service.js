import {ENV_CONFIG} from '@rigby/config';
const axios = require('axios');

/**
 * fetches the friendship status between 2 users
 * @param {*} authToken
 * @param {*} friendId
 * @returns The status in number form
 */
export async function fetchFriendShipStatus(authToken, friendId) {
  const url = `${ENV_CONFIG.HOST_BACKEND}friends/friendship_status/${friendId}`;
  const axiosConfig = {
    method: 'get',
    url: url,
    headers: {
      Authorization: authToken,
    },
  };
  try {
    const res = await axios(axiosConfig);
    const response = {
      data: res.data,
    };
    console.log(response.data);
    return response;
  } catch (error) {
    const response = {data: error.response.data.message};
    console.log(`Error: ${error.response.data.message}`);
    return response;
  }
}
