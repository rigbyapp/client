import {ENV_CONFIG} from '@rigby/config';
const axios = require('axios');
/**
 * Makes an API call for an user profile data
 * @param {String} authToken The token of the current user
 * @param {Number} userId The id of the user we are fetching
 * @return an object containing  the profle details( pfp, name, birthdate, description)
 */
export async function fetchUserData(authToken, userId) {
  const url = `${ENV_CONFIG.HOST_BACKEND}users/by_id/${userId}`;
  const axiosConfig = {
    method: 'get',
    url: url,
    headers: {
      Authorization: authToken,
    },
  };
  try {
    const res = await axios(axiosConfig);
    const response = {
      data: res.data,
    };
    //console.log(response.data);
    return response;
  } catch (error) {
    const response = {data: error.response.data.message};
    console.log(`Error on service: ${error.response.data.message}`);
    return response;
  }
}
