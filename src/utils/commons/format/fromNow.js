/**
 * Returns a string with the estimation of the time given a date from now.
 * @param {String} ISODate
 * @param {String} dateLocale
 */
export function fromNow(ISODate, dateLocale) {
  const moment = require('moment');
  return moment(ISODate).locale(dateLocale).fromNow();
}
