/**
 * Returns a Error message in the given language pack given a code
 * @param {object} languagePackErrors The 'errors' object of the language pack
 * @param {String} errorCode The error code returned from backend
 */
export function getErrorFromCode(languagePackErrors, errorCode) {
  return `${languagePackErrors[errorCode]}`;
}
