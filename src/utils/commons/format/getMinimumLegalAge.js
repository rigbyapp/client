/**
 * Returns the minimum birth date for someone to be over 18 years old
 * @return {Date} A birth date in js date format
 */
export function getMinimumLegalAge() {
  const moment = require('moment');
  return moment().subtract(18, 'years').toDate();
}
