/**
 * Returns an age given a date in ISO 8601 format in number.
 * @param {String} ISOString an ISOdate timestamp
 * @return Years in umber format.
 */
export function getAge(ISOString) {
  const moment = require('moment');
  return Math.floor(
    moment.duration({from: ISOString, to: moment().toISOString()}).asYears(),
  );
}
