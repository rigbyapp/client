import {isIphoneX} from '@rigby/commons/device/isIphoneX';
export const layout = {
  bottomAreaSize: isIphoneX() ? 34 : 20,
  topAreaSize: isIphoneX() ? 0 : 0,
};
