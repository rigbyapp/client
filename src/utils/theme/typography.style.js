import {StyleSheet} from 'react-native';

export const typography = StyleSheet.create({
  giga: {
    fontSize: 100,
    fontFamily: 'Raleway-ExtraBold',
  },
  title: {
    fontSize: 36,
    fontFamily: 'Raleway-Bold',
  },
  subtitle: {
    fontSize: 30,
    fontFamily: 'Raleway-Bold',
  },
  subtitle2: {
    fontSize: 24,
    fontFamily: 'Raleway-Bold',
  },
  subtitle3: {
    fontSize: 18,
    fontFamily: 'Raleway-Bold',
  },
  subtitle3Regular: {
    fontSize: 18,
    fontFamily: 'Raleway-Regular',
  },
  button: {
    fontSize: 15,
    fontFamily: 'Raleway-Regular',
  },
  caption: {
    fontSize: 13,
    fontFamily: 'Raleway-Regular',
  },
  quote: {
    fontSize: 15,
    fontFamily: 'Raleway-Italic',
  },
  bold: {
    fontSize: 15,
    fontFamily: 'Raleway-Bold',
  },
});
