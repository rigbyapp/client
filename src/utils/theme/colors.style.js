export const colors = {
  main: '#5423DF',
  main2: '#6D73FF',
  main3: '#09031F',
  secondary: '#9127C3',
  background: '#000000',
  backgroundOpacityZero: '#00000000',
  border1: '#4D3C73',
  text1: '#FFF',
  text2: '#cad4f2',
  text3: '#7E7DA9',
  inputBackgroundFocused: '#38384E',
  inputBackground: '#585879',
  error1: '#F28888',
  error2: '#ffe070',
  disabled: '#1C1646',
};

export const gradients = {
  gradient1: [colors.main, colors.secondary],
  gradient2: [colors.backgroundOpacityZero, colors.background],
  gradient2Inverted: [colors.background, colors.backgroundOpacityZero],
  gradient3: [colors.background, colors.main],
  gradientMainMenu: [colors.backgroundOpacityZero, colors.main3],
  gradient4: ['rgba(0, 0, 0, .8)', 'rgba(0, 0, 0, .55)', 'rgba(0, 0, 0, 0)'],
};
