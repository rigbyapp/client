//Icons
import {library} from '@fortawesome/fontawesome-svg-core';
import {
  faExclamationCircle,
  faEye,
  faEyeSlash,
  faChevronLeft,
  faCheckSquare,
} from '@fortawesome/free-solid-svg-icons';

/**
 * Imports the icon library, only needed on the root of the app
 */
export function iconLibrary() {
  library.add(
    faExclamationCircle,
    faEye,
    faEyeSlash,
    faChevronLeft,
    faCheckSquare,
  );
}
