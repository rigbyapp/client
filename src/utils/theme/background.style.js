import {StyleSheet} from 'react-native';
import {colors} from './colors.style';

export const globalStyles = StyleSheet.create({
  globalBackground: {
    backgroundColor: colors.background,
  },
});
