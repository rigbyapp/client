import React, {useState, useEffect, useContext} from 'react';
import {View} from 'react-native';
//Styles
import {styles} from './Profile.style';
//Context
import {UserContext, GeneralConfigContext} from '@rigby/components/context';
//functionality
import {fetchProfileFeed} from './Profile.func';
//Screen components
import {ProfileFeed} from './components/ProfileFeed';
import {ProfileData} from './components/ProfileData';

export const Profile = (props) => {
  const user = useContext(UserContext);
  const appConfig = useContext(GeneralConfigContext);
  const [profileId, setProfileId] = useState(props.route.params.userId);

  const [feed, setFeed] = useState();
  //Lifecycle
  useEffect(() => {
    setProfileId(props.route.params.userId);
    fetchProfileFeed(user, profileId, appConfig, setFeed);
  }, [setFeed, setProfileId, profileId, props.route.params.userId]);
  return (
    <View style={styles.contentWrapper}>
      <ProfileData profileId={profileId} />
      <ProfileFeed feed={feed} />
    </View>
  );
};
