import {StyleSheet} from 'react-native';
import {colors} from '@rigby/theme/colors.style';

export const styles = StyleSheet.create({
  contentWrapper: {
    flex: 1,
    backgroundColor: colors.background,
    paddingTop: 10,
  },
  feedWrapper: {
    flexDirection: 'row',
  },
});
