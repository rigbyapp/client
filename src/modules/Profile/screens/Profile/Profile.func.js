//Services
import {fetchUserFeed} from '@rigby/commons/services/userFeed.service';
import {fetchUserData} from '@rigby/commons/services/userData.service';
import {fetchFriendShipStatus} from '@rigby/commons/services/friendShipStatus.service';
import {getAge} from '@rigby/commons/format/getAge';

/**
 * Asigns a copie given a friendship status
 * @param {*} friendshipStatus
 * @param {*} copies
 * @param {*} setCTA
 */
function getCopiesfromStatus(friendshipStatus, copies, setCTA) {
  switch (friendshipStatus) {
    case friendshipStatus < 2:
      setCTA(copies.addFriend);
      break;
    case 2:
      setCTA(copies.cancelRequest);
      break;
    case 3:
      setCTA(copies.acceptRequest);
      break;
    case 4:
      setCTA(copies.deleteFriend);
      break;
    default:
      setCTA(copies.addFriend);
      break;
  }
}

/**
 * Fetches the post of a given user
 * @param {Object} user The global user context
 * @param {Number} otherUserId The id of the user we are fetching
 * @param {Object} config The global config context
 * @param {State Hook} setFeedHook The state hook for setting the feed
 */
export async function fetchProfileFeed(user, otherUserId, config, setFeedHook) {
  config.setLoading(true);
  try {
    const response = await fetchUserFeed(user.token, otherUserId);
    //Splice Array in columns of 3
    var row = [],
      size = 3;
    while (response.data.length > 0) row.push(response.data.splice(0, size));
    setFeedHook(row);
    //console.log(response.data[0]);
  } catch (error) {
    console.log(error);
  }
  config.setLoading(false);
}
/**
 * Fetches a given profile details (Profile photo, name, age, description)
 * @param {*} user The global user context
 * @param {*} otherUserId The id of the user we are fetching
 * @param {*} config The global config context
 * @param {*} setDataHook The state hook for setting the data
 * @param {*} setAge The state hook for setting the age
 * @param {*} setFriendStatusHook The state hook for setting the firendship status between the user and the profile visited
 */
export async function fetchProfileData(
  user,
  otherUserId,
  config,
  setDataHook,
  setAge,
  setFriendStatusHook,
  setMainCTAHook,
) {
  config.setLoading(true);
  try {
    const dataResponse = await fetchUserData(user.token, otherUserId);
    setDataHook(dataResponse.data);
    setAge &&
      setAge(
        `${getAge(dataResponse.data.birthDate)} ${config.copies.yearsOld}`,
      );
    const statusResponse = await fetchFriendShipStatus(user.token, otherUserId);
    setFriendStatusHook(statusResponse.data.status);
    getCopiesfromStatus(
      statusResponse.data.status,
      config.copies,
      setMainCTAHook,
    );
    //console.log(response.data);
  } catch (error) {
    console.log(error);
  }
  config.setLoading(false);
}
