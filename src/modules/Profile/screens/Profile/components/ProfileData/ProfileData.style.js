import {StyleSheet} from 'react-native';
import {colors} from '@rigby/theme/colors.style';

export const styles = StyleSheet.create({
  innerWrapper: {
    marginTop: 20,
    paddingHorizontal: 20,
    paddingBottom: 30,
    borderBottomWidth: 1,
    borderBottomColor: colors.main,
  },
  profileDetails: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  profilePicture: {
    backgroundColor: colors.main,
    alignSelf: 'center',
  },
  profileData: {
    marginLeft: 15,
    justifyContent: 'center',
    //backgroundColor: 'blue',
    height: 100,
  },
  dataSeparator: {
    borderLeftWidth: 1,
    borderLeftColor: colors.text1,
    marginHorizontal: 5,
    height: 10,
  },
  profileSubData: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15,
  },
});
