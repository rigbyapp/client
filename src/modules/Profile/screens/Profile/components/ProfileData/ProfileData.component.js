import React, {useState, useEffect, useContext} from 'react';
import {View} from 'react-native';
//Styles
import {styles} from './ProfileData.style';
//Model
import {model} from './ProfileData.model';
//Rigby
import {Typography} from '@rigby/components/text/Typography';
import {BaseImage} from '@rigby/components/media/BaseImage';
import {Button} from '@rigby/components/inputs/Button';
//functionality
import {fetchProfileData} from '../../Profile.func';
//Context
import {UserContext, GeneralConfigContext} from '@rigby/components/context';
export const ProfileData = (props) => {
  //Local state
  const [age, setAge] = useState('');
  const [data, setData] = useState({});
  const [friendshipStatus, setFriendShipStatus] = useState(0);
  //Global state
  const appConfig = useContext(GeneralConfigContext);
  const user = useContext(UserContext);
  const [mainCTA, setMainCTA] = useState(' ');
  //Lifecycle
  useEffect(() => {
    fetchProfileData(
      user,
      props.profileId,
      appConfig,
      setData,
      setAge,
      setFriendShipStatus,
      setMainCTA,
    );
  }, [props.profileId]);
  return (
    <View style={styles.innerWrapper}>
      <View style={styles.profileDetails}>
        <BaseImage
          width={100}
          height={100}
          style={styles.profilePicture}
          forceLoadState={appConfig.loading}
          borderRadius={5}
          source={{
            uri: data.profilePicture,
          }}
        />
        <View style={styles.profileData}>
          <Typography
            variant={'subtitle3'}
            style={styles.name}
            forceLoadState={appConfig.loading}
            skeletonWidth={150}>
            {data.firstName} {data.lastName}
          </Typography>
          <View style={styles.profileSubData}>
            <Typography
              color={'text2'}
              forceLoadState={appConfig.loading}
              skeletonWidth={75}>
              {age}
            </Typography>
            {props.user.occupation && (
              <>
                <View style={styles.dataSeparator} />
                <Typography
                  forceLoadState={appConfig.loading}
                  skeletonWidth={100}>
                  {props.user.occupation}
                </Typography>
              </>
            )}
          </View>
          {user.userId !== props.profileId && (
            <Button.Gradient
              style={{marginBottom: 0}}
              width={120}
              forceLoadState={appConfig.loading}>
              {mainCTA}
            </Button.Gradient>
          )}
        </View>
      </View>
      <View>
        {user.description && <Typography>{user.description}</Typography>}
      </View>
    </View>
  );
};
ProfileData.propTypes = {
  ...model.types,
};

ProfileData.defaultProps = {
  ...model.default,
};
