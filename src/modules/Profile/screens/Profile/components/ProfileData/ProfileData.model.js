/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    user: PropTypes.object.isRequired,
    appConfig: PropTypes.object.isRequired,
    profileId: PropTypes.number,
  },
  default: {
    user: {},
    appConfig: {},
  },
};
