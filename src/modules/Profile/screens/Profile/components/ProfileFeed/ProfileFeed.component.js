import React, {useState, useContext} from 'react';
import {View, Dimensions, FlatList} from 'react-native';
//Style
import {styles} from './ProfileFeed.style';
//Model
import {model} from './ProfileFeed.model';
//Rigby
import {Notice} from '@rigby/components/informatives/Notice';
import {FeedRow} from '../FeedRow';
//Context
import {GeneralConfigContext} from '@rigby/components/context';

export const ProfileFeed = (props) => {
  //Local state
  const DIM_WIDTH = Dimensions.get('window').width;
  const thumbnailDim = DIM_WIDTH / 3;
  const [modal, setModal] = useState();
  //Global state
  const appConfig = useContext(GeneralConfigContext);

  const ItemRender = ({item}) => <FeedRow row={item} />;

  return (
    <View>
      {props.feed[0] ? (
        <>
          <FlatList
            style={styles.feedList}
            data={props.feed}
            renderItem={ItemRender}
            keyExtractor={(item) => item.postId}
            bounces={false}
          />
          {modal}
        </>
      ) : (
        <Notice message={appConfig.copies.theresNoPosts} />
      )}
    </View>
  );
};
ProfileFeed.propTypes = {
  ...model.types,
};

ProfileFeed.defaultProps = {
  ...model.default,
};
