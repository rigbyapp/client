/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    feed: PropTypes.array,
  },
  default: {
    feed: [],
  },
};
