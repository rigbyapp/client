import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  feedList: {
    height: '78%',
    paddingBottom: 10,
  },
  thumbnail: {
    resizeMode: 'cover',
  },
});
