import React, {useContext} from 'react';
import {TouchableOpacity, View, Dimensions} from 'react-native';
//Styles
import {styles} from './FeedRow.style';
//Model
import {model} from './FeedRow.model';
//Rigby
import {BaseImage} from '@rigby/components/media/BaseImage';
import {redirectToPost} from '@rigby/utils/commons/navigation/redirectToPost';
import {fetchProfileData} from '../../Profile.func';
//Context
import {GeneralConfigContext} from '@rigby/components/context';

export const FeedRow = (props) => {
  const DIM_WIDTH = Dimensions.get('window').width;
  const thumbnailDim = DIM_WIDTH / 3;
  //Global state
  const appConfig = useContext(GeneralConfigContext);

  return (
    <View style={styles.rowWrapper}>
      {props.row.map(function (post, k) {
        return (
          <TouchableOpacity
            key={k}
            onPress={() => {
              redirectToPost(post, appConfig, fetchProfileData);
            }}>
            <BaseImage
              source={{uri: post.thumbnail}}
              style={{
                ...styles.thumbnail,
              }}
              width={thumbnailDim}
              height={thumbnailDim}
              borderRadius={0}
            />
          </TouchableOpacity>
        );
      })}
    </View>
  );
};
FeedRow.propTypes = {
  ...model.types,
};

FeedRow.defaultProps = {
  ...model.default,
};
