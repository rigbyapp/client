/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    row: PropTypes.array.isRequired,
  },
  default: {
    row: [],
  },
};
