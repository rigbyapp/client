import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
//Import by flows
import {OnBoarding, Login, SignUp} from '@rigby/modules/OnBoarding';
import {Home, Meet, Friends} from '@rigby/modules/Main';
import {Profile} from '@rigby/modules/Profile';
//Menus
import {MainMenu} from '@rigby/components/navigation/MainMenu';

const Stack = createStackNavigator();
const Menu = createBottomTabNavigator();

//Stack divided by flows
const OnBoardingFlow = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName={'OnBoarding'}>
      <Stack.Screen name="OnBoarding" component={OnBoarding} />
      <Stack.Screen name="SignUp" component={SignUp} />
      <Stack.Screen name="Login" component={Login} />
    </Stack.Navigator>
  );
};

const MainFlow = () => {
  return (
    <Menu.Navigator
      screenOptions={{headerShown: false}}
      gesturesEnabled={true}
      swipeEnabled={true}
      tabBar={MainMenu}
      initialRouteName={'Home'}>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Meet" component={Meet} />
      <Stack.Screen name="Friends" component={Friends} />
    </Menu.Navigator>
  );
};

const ProfileFlow = () => {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName={'Profile'}>
      <Stack.Screen name="Profile" component={Profile} />
    </Stack.Navigator>
  );
};

export const AppRouter = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false, gestureEnabled: false}}
        initialRouteName={'OnBoardingFlow'}>
        <Stack.Screen name="OnBoardingFlow" component={OnBoardingFlow} />
        <Stack.Screen name="MainFlow" component={MainFlow} />
        <Stack.Screen name="ProfileFlow" component={ProfileFlow} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
