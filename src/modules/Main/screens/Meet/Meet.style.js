import {StyleSheet} from 'react-native';
import {colors} from '@rigby/theme/colors.style';

export const styles = StyleSheet.create({
  contentWrapper: {
    flex: 1,
    backgroundColor: colors.background,
    justifyContent: 'flex-end',
  },
  info: {
    width: 200,
    marginBottom: 30,
    marginLeft: 20,
    marginTop: 40,
  },
  listItem: {
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderBottomColor: colors.border1,
    borderTopColor: colors.border1,
    paddingHorizontal: 20,
    paddingBottom: 20,
  },
  listItemInner: {
    flexDirection: 'row',
  },
  profilePhoto: {
    width: 60,
    height: 60,
    backgroundColor: colors.secondary,
    borderRadius: 5,
    marginRight: 20,
  },
  date: {
    paddingTop: 5,
    height: 20,
    alignItems: 'flex-end',
  },
  quoteWrapper: {
    marginTop: 5,
  },
  briefWrapper: {
    justifyContent: 'center',
  },
});
