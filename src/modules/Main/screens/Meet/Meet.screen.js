import React, {useState, useEffect, useContext} from 'react';
import {SafeAreaView, FlatList, useWindowDimensions, View} from 'react-native';
//Styles
import {styles} from './Meet.style';
//Components
import {Typography} from '@rigby/components/text/Typography';
import {Card} from '@rigby/components/layout/Card';
//Functionality
import {fetchSuggestionsList, handleCardPress} from './Meet.func';
//Context
import {UserContext, GeneralConfigContext} from '@rigby/components/context';
import {getAge} from '@rigby/utils/commons/format';

export const Meet = (props) => {
  //Local state
  const [feedItems, setFeedItems] = useState([]);
  //Global state
  const user = useContext(UserContext);
  const appConfig = useContext(GeneralConfigContext);
  useEffect(() => {
    fetchSuggestionsList(user, appConfig, setFeedItems);
  }, []);

  const ItemRender = ({item}) => (
    <Card.Profile
      onPress={() =>
        handleCardPress(appConfig.appNavigation, props.navigation, item.userId)
      }
      name={item.firstName}
      lastName={item.lastName}
      age={getAge(item.birthDate)}
      time={'hace 2 horas'}
      image={item.profilePicture}
    />
  );
  return (
    <SafeAreaView style={styles.contentWrapper}>
      <View>
        <FlatList
          horizontal
          data={feedItems}
          renderItem={ItemRender}
          keyExtractor={(item) => item.userId}
        />
        <View style={styles.info}>
          <Typography>
            Desliza hacia arriba para mandar una solicitud.
          </Typography>
        </View>
      </View>
    </SafeAreaView>
  );
};
