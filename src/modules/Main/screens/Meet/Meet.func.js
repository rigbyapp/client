//Services
import {fetchFriendSuggestions} from '@rigby/commons/services/friendSuggestions.service';
/**
 * Fetches the friend suggestions for the given user
 * @param {*} user The user context
 * @param {*} config The global app config cpntext
 * @param {*} setFeedHook The hook for setting the feed
 */
export async function fetchSuggestionsList(user, config, setFeedHook) {
  config.setLoading(true);
  try {
    const response = await fetchFriendSuggestions(user.token);
    setFeedHook(response.data);
    console.log(response.data);
  } catch (error) {
    console.log(error);
  }
  config.setLoading(false);
}

/**
 * Handles the press of profile card
 * @param {*} appNavigation The global app navigation function
 * @param {*} navigationProps The navigation props from react-navigation
 * @param {*} userId The id of the user to navigate to
 */
export function handleCardPress(appNavigation, navigationProps, userId) {
  appNavigation(navigationProps, 'ProfileFlow', {
    screen: 'Profile',
    params: {userId: userId},
  });
}
