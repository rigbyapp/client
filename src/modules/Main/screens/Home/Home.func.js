//Services
import {fetchNotifications} from '@rigby/commons/services/notifications.service';
/**
 * Fetches the friend suggestions for the given user
 * @param {*} user The user context
 * @param {*} config The global app config cpntext
 * @param {*} setFeedHook The hook for setting the feed
 */
export async function fetchNotificationsList(user, config, setFeedHook) {
  config.setLoading(true);
  try {
    const response = await fetchNotifications(user.token);
    setFeedHook(response.data);
    console.log(response.data);
  } catch (error) {
    console.log(error);
  }
  config.setLoading(false);
}

/**
 * Handles the press of an item list
 * @param {*} appNavigation The global app navigation function
 * @param {*} navigationProps The navigation props from react-navigation
 * @param {*} userId The id of the user to navigate to
 */
export function handleItemPress(appNavigation, navigationProps, userId) {
  appNavigation(navigationProps, 'ProfileFlow', {
    screen: 'Profile',
    params: {userId: userId},
  });
}
