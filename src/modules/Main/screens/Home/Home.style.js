import {StyleSheet} from 'react-native';
import {colors} from '@rigby/theme/colors.style';

export const styles = StyleSheet.create({
  contentWrapper: {
    flex: 1,
    backgroundColor: colors.background,
    justifyContent: 'flex-end',
    marginTop: 25,
  },
  flatList: {
    minHeight: '100%',
  },
});
