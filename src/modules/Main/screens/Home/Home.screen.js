import React, {useState, useEffect, useContext} from 'react';
import {SafeAreaView, FlatList, View} from 'react-native';
//Styles
import {styles} from './Home.style';
//Functionality
import {fetchNotificationsList, handleItemPress} from './Home.func';
import {fromNow} from '@rigby/utils/commons/format/fromNow.js';
//Rigby
import {ListItem} from '@rigby/components/layout/ListItem';
import axios from 'axios';
//Context
import {UserContext, GeneralConfigContext} from '@rigby/components/context';

export const Home = (props) => {
  //Local state
  const [feedItems, setFeedItems] = useState([]);
  //Global state
  const user = useContext(UserContext);
  const appConfig = useContext(GeneralConfigContext);
  useEffect(() => {
    fetchNotificationsList(user, appConfig, setFeedItems);
  }, []);

  const ItemRender = ({item}) => (
    <ListItem.Notification
      firstName={item.senderFirstName}
      lastName={item.senderLastName}
      description={item.description}
      time={fromNow(item.timestamp, appConfig.copies.dateLocale)}
      image={item.senderProfilePicture}
      senderId={item.senderId}
      onPress={() => {
        handleItemPress(
          appConfig.appNavigation,
          props.navigation,
          item.senderId,
        );
      }}
    />
  );
  return (
    <SafeAreaView style={styles.contentWrapper}>
      <View>
        <FlatList
          data={feedItems}
          renderItem={ItemRender}
          keyExtractor={(item) => item.email}
          style={styles.flatList}
        />
      </View>
    </SafeAreaView>
  );
};
