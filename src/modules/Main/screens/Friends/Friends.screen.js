import React, {useState, useEffect, useContext} from 'react';
import {SafeAreaView, FlatList, View} from 'react-native';
//Styles
import {styles} from './Friends.style';
//Components
import {ListItem} from '@rigby/components/layout/ListItem';
import {UserContext, GeneralConfigContext} from '@rigby/components/context';
//Functionality
import {handleFecthing, handleItemPress} from './Friends.func';

export const Friends = (props) => {
  //Global Sate
  const user = useContext(UserContext);
  const appConfig = useContext(GeneralConfigContext);
  //Local State
  const [feedItems, setFeedItems] = useState([]);
  useEffect(() => {
    handleFecthing(user.token, feedItems, setFeedItems, appConfig);
  }, []);

  const ItemRender = ({item}) => (
    <ListItem.Friend
      name={item.firstName}
      lastName={item.lastName}
      image={item.profilePicture}
      onPress={() =>
        handleItemPress(appConfig.appNavigation, props.navigation, item.userId)
      }
    />
  );
  return (
    <SafeAreaView style={styles.contentWrapper}>
      <View>
        <FlatList
          data={feedItems}
          renderItem={ItemRender}
          keyExtractor={(item) => item.userId.toString()}
          style={styles.flatList}
        />
      </View>
    </SafeAreaView>
  );
};
