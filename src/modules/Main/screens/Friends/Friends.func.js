//Services
import {fetchFriends} from '@rigby/commons/services/friends.service';
/**
 * Fetches all the user's fiends
 * @param {*} authToken The token of the user
 * @param {*} feedItems The feedItems hook
 * @param {*} setFeedItems The set hook for feddItems
 * @param {*} config The general app config
 */
export async function handleFecthing(
  authToken,
  feedItems,
  setFeedItems,
  config,
) {
  config.setLoading(true);
  try {
    const response = await fetchFriends(authToken);
    setFeedItems(response.data);
    console.log(response);
  } catch (error) {
    console.log(error);
  }
  config.setLoading(false);
}

/**
 * Handles the press of an item list
 * @param {*} appNavigation The global app navigation function
 * @param {*} navigationProps The navigation props from react-navigation
 * @param {*} userId The id of the user to navigate to
 */
export function handleItemPress(appNavigation, navigationProps, userId) {
  appNavigation(navigationProps, 'ProfileFlow', {
    screen: 'Profile',
    params: {userId: userId},
  });
}
