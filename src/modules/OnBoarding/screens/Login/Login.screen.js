import React, {useState, useContext, useEffect} from 'react';
import {View, SafeAreaView, KeyboardAvoidingView} from 'react-native';
//Styles
import {styles} from './Login.style';
//Global Components
import {FadeInView} from '@rigby/components/microInteractions/FadeInView';
import {Button} from '@rigby/components/inputs/Button';
import {TInput} from '@rigby/components/inputs/TInput';
import {Typography} from '@rigby/components/text/Typography';
import {UserContext, GeneralConfigContext} from '@rigby/components/context';
import {SectionScreen} from '@rigby/components//layout/SectionScreen';
import {Overlay} from '@rigby/components/layout/Overlay';
//Functionality
import {handleLogin} from './Login.func';
import {validation} from '@rigby/commons/validation';

export const Login = (props) => {
  //Global Sate
  const user = useContext(UserContext);
  const appConfig = useContext(GeneralConfigContext);
  //Local State
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [formError, setFormError] = useState(true);
  const [submitError, setSubmitError] = useState('');

  useEffect(() => {
    if (!validation.email(email) || !validation.filled(password)) {
      setFormError(true);
    } else {
      setFormError(false);
    }
  }, [setFormError, email, password]);

  return (
    <SafeAreaView style={styles.contentWrapper}>
      <SectionScreen
        onPressBack={() =>
          appConfig.appNavigationWithoutHistory(props.navigation, 'OnBoarding')
        }
        title={appConfig.copies.login}>
        <View style={styles.form}>
          <KeyboardAvoidingView>
            <FadeInView duration={800} delay={400}>
              <TInput.Mail
                placeHolder={appConfig.copies.mail}
                onChangeText={setEmail}
                value={email}
                editable={!appConfig.loading}
                errorMessage={appConfig.copies.validEmail}
              />
              <TInput.Password
                placeHolder={appConfig.copies.password}
                secureTextEntry={true}
                onChangeText={setPassword}
                value={password}
                validationType={'filled'}
                errorMessage={appConfig.copies.fieldRequiered}
                editable={!appConfig.loading}
              />
              <Button.Gradient
                onPress={() =>
                  handleLogin(
                    email,
                    password,
                    props.navigation,
                    user,
                    appConfig,
                    setSubmitError,
                  )
                }
                disabled={formError}>
                {appConfig.copies.login}
              </Button.Gradient>
              {submitError ? (
                <Typography color={'error2'}>{submitError}</Typography>
              ) : null}
            </FadeInView>
          </KeyboardAvoidingView>
        </View>
        <FadeInView duration={400} delay={1000}>
          <Typography>
            {appConfig.copies.dontHaveAnAccount[0]}
            <Button.Link onPress={() => props.navigation.navigate('SignUp')}>
              {appConfig.copies.dontHaveAnAccount[1]}
            </Button.Link>
          </Typography>
        </FadeInView>
      </SectionScreen>
      {appConfig.loading ? <Overlay /> : null}
    </SafeAreaView>
  );
};
