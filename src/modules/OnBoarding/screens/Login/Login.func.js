//Services
import {loginService} from '@rigby/commons/services/login.service';
import {getErrorFromCode} from '@rigby/commons/format';

export async function handleLogin(
  userEmail,
  userPassword,
  navigationProps,
  user,
  appConfig,
  setSubmitError,
) {
  appConfig.setLoading(true);
  try {
    const response = await loginService(userEmail, userPassword);
    console.log(userEmail, userPassword);
    console.log(response);
    if (response.auth) {
      //Set user global state
      user.setToken(response.token);
      user.setFirstName(response.data.firstName);
      user.setLastName(response.data.lastName);
      user.setProfilePicture(response.data.profilePicture);
      user.setZipcode(response.data.zipcode);
      user.setBirthDate(response.data.birthDate);
      user.setIsLogged(response.auth);
      user.setUserId(response.data.userId);
      appConfig.appNavigation(navigationProps, 'MainFlow', {
        screen: 'Home',
      });
    } else {
      setSubmitError(
        `${appConfig.copies.thereWasAnIssue} \n ${getErrorFromCode(
          appConfig.copies.errors,
          response.data,
        )}`,
      );
    }
    appConfig.setLoading(false);
  } catch (error) {
    setSubmitError(
      `${appConfig.copies.thereWasAnIssue} \n ${appConfig.copies.errors.NO_CONNECTION}`,
    );
  }
  appConfig.setLoading(false);
}
