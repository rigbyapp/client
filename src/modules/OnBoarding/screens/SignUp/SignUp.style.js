import {StyleSheet} from 'react-native';
import {colors} from '@rigby/theme/colors.style';

export const styles = StyleSheet.create({
  contentWrapper: {
    flex: 1,
    backgroundColor: colors.background,
  },
  innerWrapper: {
    paddingHorizontal: 20,
  },
  form: {
    marginBottom: 30,
  },
  disclaimer: {
    marginBottom: 30,
  },
  bottomInputs: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  bottomInputWrapper: {
    flex: 1,
  },
  bottomInputFirst: {
    marginRight: 20,
  },
});
