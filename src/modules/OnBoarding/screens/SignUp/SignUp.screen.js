import React, {useState, useContext, useEffect} from 'react';
import {View, SafeAreaView, TouchableOpacity} from 'react-native';
//Styles
import {styles} from './SignUp.style';
//Global Components
import {FadeInView} from '@rigby/components/microInteractions/FadeInView';
import {Button} from '@rigby/components/inputs/Button';
import {TInput} from '@rigby/components/inputs/TInput';
import {Typography} from '@rigby/components/text/Typography';
import {Overlay} from '@rigby/components/layout/Overlay';
import {Checkbox} from '@rigby/components/inputs/Checkbox';
//Context
import {GeneralConfigContext, UserContext} from '@rigby/components/context';
//functionality
import {handleSignUp} from './Signup.func';
import {validation} from '@rigby/commons/validation';
import {SectionScreen} from '@rigby/components//layout/SectionScreen';
import {getMinimumLegalAge} from '@rigby/commons/format';

export const SignUp = (props) => {
  //global state
  const user = useContext(UserContext);
  const appConfig = useContext(GeneralConfigContext);

  //local state
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [formError, setFormError] = useState(true);
  const [submitError, setSubmitError] = useState('');
  const [zipCode, setZipCode] = useState('');
  const [birthDate, setBirthDate] = useState(getMinimumLegalAge());
  const [over18, setOver18] = useState(false);

  useEffect(() => {
    if (
      !validation.email(email) ||
      !validation.password(password) ||
      !validation.filled(firstName) ||
      !validation.filled(lastName) ||
      !validation.zipCode(zipCode, appConfig.locale) ||
      !over18
    ) {
      setFormError(true);
    } else {
      setFormError(false);
    }
  }, [
    setFormError,
    email,
    password,
    firstName,
    lastName,
    zipCode,
    appConfig.locale,
    birthDate,
    over18,
  ]);
  return (
    <SafeAreaView style={styles.contentWrapper}>
      <SectionScreen
        onPressBack={() =>
          appConfig.appNavigationWithoutHistory(props.navigation, 'OnBoarding')
        }
        title={appConfig.copies.signup}>
        <View style={styles.form}>
          <FadeInView duration={800} delay={400}>
            <TInput.Mail
              placeHolder={appConfig.copies.mail}
              onChangeText={setEmail}
              value={email}
              editable={!appConfig.loading}
              errorMessage={appConfig.copies.validEmail}
            />
            <TInput.Password
              placeHolder={appConfig.copies.password}
              secureTextEntry={true}
              onChangeText={setPassword}
              value={password}
              validationType={'password'}
              editable={!appConfig.loading}
              errorMessage={appConfig.copies.validPassword}
            />
            <TInput.Single
              placeHolder={appConfig.copies.firstName}
              onChangeText={setFirstName}
              value={firstName}
              error={validation.empty(firstName)}
              validationType={'name'}
              errorMessage={appConfig.copies.fieldIsRequiered}
              editable={!appConfig.loading}
            />
            <TInput.Single
              placeHolder={appConfig.copies.lastName}
              onChangeText={setLastName}
              value={lastName}
              validationType={'name'}
              errorMessage={appConfig.copies.fieldIsRequiered}
              editable={!appConfig.loading}
            />
            <View style={styles.bottomInputs}>
              <View
                style={{
                  ...styles.bottomInputWrapper,
                  ...styles.bottomInputFirst,
                }}>
                <TInput.ZipCode
                  placeHolder={appConfig.copies.zipCode}
                  onChangeText={setZipCode}
                  value={zipCode}
                  editable={!appConfig.loading}
                  countryCode={appConfig.locale}
                  errorMessage={appConfig.copies.validZipCode}
                />
              </View>
              <View style={styles.bottomInputWrapper}>
                <TInput.Date
                  date={birthDate}
                  placeHolder={appConfig.copies.birthDate}
                  locale={appConfig.copies.dateLocale}
                  maxDate={getMinimumLegalAge()}
                  editable={!appConfig.loading}
                  onSelectDate={setBirthDate}
                />
              </View>
            </View>
            <Checkbox
              description={appConfig.copies.over18}
              onPress={() => setOver18(!over18)}
            />
            <Button.Gradient
              onPress={() =>
                handleSignUp(
                  email,
                  password,
                  firstName,
                  lastName,
                  zipCode,
                  birthDate,
                  props.navigation,
                  user,
                  appConfig,
                  setSubmitError,
                )
              }
              disabled={formError}>
              {appConfig.copies.createYourAccount}
            </Button.Gradient>
            {submitError ? (
              <Typography color={'error2'}>{submitError}</Typography>
            ) : null}
          </FadeInView>
        </View>
        <FadeInView duration={800} delay={800} style={styles.disclaimer}>
          <Typography>
            {appConfig.copies.signupDisclaimer[0]}
            <Typography variant={'bold'}>
              {appConfig.copies.signupDisclaimer[1]}
            </Typography>
            {appConfig.copies.signupDisclaimer[2]}
            <Typography variant={'bold'}>
              {appConfig.copies.signupDisclaimer[3]}
            </Typography>
          </Typography>
        </FadeInView>
      </SectionScreen>

      {appConfig.loading ? <Overlay /> : null}
    </SafeAreaView>
  );
};
