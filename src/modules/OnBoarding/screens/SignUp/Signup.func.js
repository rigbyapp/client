//Services
import {signUpService} from '@rigby/commons/services/signUp.service';
import {getErrorFromCode} from '@rigby/commons/format';

export async function handleSignUp(
  userEmail,
  userPassword,
  userFirstName,
  userLastName,
  userZipCode,
  userBirthDate,
  navigationProps,
  user,
  appConfig,
  setSubmitError,
) {
  appConfig.setLoading(true);
  try {
    console.log(
      userEmail,
      userPassword,
      userFirstName,
      userLastName,
      userZipCode,
      userBirthDate,
    );
    const response = await signUpService(
      userEmail,
      userPassword,
      userFirstName,
      userLastName,
      userZipCode,
      userBirthDate,
    );
    console.log(response);
    if (response.auth) {
      //Set user global state
      user.setToken(response.token);
      user.setIsLogged(response.auth);
      user.setFirstName(response.data.firstName);
      user.setLastName(response.data.lastName);
      user.setProfilePicture(response.data.profilePicture);
      user.setZipcode(response.data.zipcode);
      user.setBirthDate(response.data.birthDate);
      user.setUserId(response.data.userId);
      appConfig.appNavigation(navigationProps, 'MainFlow', {
        screen: 'Meet',
      });
    } else {
      setSubmitError(
        `${appConfig.copies.thereWasAnIssue} \n ${getErrorFromCode(
          appConfig.copies.errors,
          response.data,
        )}`,
      );
    }
    appConfig.setLoading(false);
  } catch (error) {
    setSubmitError(
      `${appConfig.copies.thereWasAnIssue} \n ${appConfig.copies.errors.NO_CONNECTION}`,
    );
  }
  appConfig.setLoading(false);
}
