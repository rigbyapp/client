import {StyleSheet} from 'react-native';
import {layout} from '@rigby/theme/layout.style';
import {colors} from '@rigby/theme/colors.style';

export const styles = StyleSheet.create({
  contentWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.background,
    paddingBottom: layout.bottomAreaSize,
  },
  logo: {
    width: 155,
    resizeMode: 'contain',
    height: 67.7,
  },
  logoContainer: {
    marginBottom: 60,
    zIndex: 1,
    elevation: 2,
    width: 184,
    alignItems: 'center',
  },
  buttonContainer: {
    zIndex: 0,
  },
});
