import React, {useContext} from 'react';
import {View, Image} from 'react-native';
//Assets
import logoWhite from '@rigby/assets/logo/logo_white.png';
//Styles
import {styles} from './OnBoarding.style';
//Global Components
import {FadeInView} from '@rigby/components/microInteractions/FadeInView';
import {PopUpView} from '@rigby/components/microInteractions/PopUpView/PopUpView.component';
import {Button} from '@rigby/components/inputs/Button';
//Context
import {GeneralConfigContext} from '@rigby/components/context';

export const OnBoarding = (props) => {
  const appConfig = useContext(GeneralConfigContext);
  return (
    <View style={styles.contentWrapper}>
      <PopUpView distance={128} style={styles.logoContainer} duration={1000}>
        <Image source={logoWhite} style={styles.logo} />
      </PopUpView>
      <FadeInView duration={900}>
        <Button.Gradient
          onPress={() => {
            appConfig.appNavigationWithoutHistory(props.navigation, 'SignUp');
          }}>
          {appConfig.copies.signup}
        </Button.Gradient>
        <Button.Ghost
          onPress={() =>
            appConfig.appNavigationWithoutHistory(props.navigation, 'Login')
          }>
          {appConfig.copies.login}
        </Button.Ghost>
      </FadeInView>
    </View>
  );
};
