/********** Model of this component ************/
import PropTypes from 'prop-types';
import {ENV_CONFIG} from '@rigby/config';

export const model = {
  types: {
    handleProfilePress: PropTypes.func,
    profilePicture: PropTypes.string,
    visible: PropTypes.bool,
  },
  default: {
    visible: true,
    profilePicture: `${ENV_CONFIG.PUBLIC_BUCKET}/ry_default.jpg`,
  },
};
