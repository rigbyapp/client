import React, {useContext, useEffect, useState} from 'react';
import {View, TouchableOpacity} from 'react-native';
//Style
import {styles} from '../Header.style';
//Model
import {model} from './MainHeader.model';
//Rigby
import {BaseImage} from '@rigby/components/media/BaseImage';
import {UserContext} from '@rigby/components/context';
//ENV
import {ENV_CONFIG} from '@rigby/config';
/**
 * The header variant of the main flow
 * @param {*} props
 */
export const MainHeader = (props) => {
  //Global State
  const user = useContext(UserContext);
  //Local State
  const [profilePicture, setProfilePicture] = useState(
    `${ENV_CONFIG.PUBLIC_BUCKET}/ry_default.jpg`,
  );
  useEffect(() => {
    setProfilePicture(user.profilePicture);
  }, [setProfilePicture, user.profilePicture]);
  return (
    <View
      style={{...styles.profileBar, display: props.visible ? 'flex' : 'none'}}>
      <View style={styles.innerContent}>
        <TouchableOpacity onPress={() => props.handleProfilePress()}>
          <BaseImage
            height={35}
            width={35}
            borderRadius={5}
            source={{uri: profilePicture}}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};
MainHeader.propTypes = {
  ...model.types,
};

MainHeader.defaultProps = {
  ...model.default,
};
