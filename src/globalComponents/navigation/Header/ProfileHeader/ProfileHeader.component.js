import React from 'react';
import {View} from 'react-native';
//Style
import {styles} from '../Header.style';
//Model
import {model} from '../Header.model';
//Rigby
import {Button} from '@rigby/components/inputs/Button';
/**
 * The header variant of the profile flow
 * @param {*} props
 */
export const ProfileHeader = (props) => {
  return (
    <View
      style={{
        ...styles.profileInnerContent,
        display: props.visible ? 'flex' : 'none',
      }}>
      <Button.Icon onPress={props.onPressBack} />
    </View>
  );
};
ProfileHeader.propTypes = {
  ...model.types,
};

ProfileHeader.defaultProps = {
  ...model.default,
};
