import {StyleSheet} from 'react-native';
import {layout} from '@rigby/theme/layout.style';
import {colors} from '@rigby/theme/colors.style';

export const styles = StyleSheet.create({
  safeAreaWrapper: {
    backgroundColor: colors.background,
    zIndex: 23,
  },

  innerWrapper: {
    position: 'relative',
    overflow: 'visible',
    height: 20,
  },

  header: {
    position: 'relative',
    overflow: 'visible',
    backgroundColor: colors.background,
  },
  notchBar: {
    height: layout.topAreaSize,
  },
  profileBar: {
    overflow: 'visible',
    position: 'relative',
  },
  bottomGradient: {
    height: 25,
    width: '100%',
  },
  innerContent: {
    paddingVertical: 5,
    paddingHorizontal: 15,
    alignItems: 'flex-end',
  },
  profileInnerContent: {
    paddingTop: 5,
    paddingHorizontal: 15,
  },
});
