import React, {useContext} from 'react';
import {SafeAreaView, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
//Style
import {styles} from './Header.style';
//Model
import {model} from './Header.model';
//Rigby
import {gradients} from '@rigby/theme/colors.style';
import {LoadingBar} from '@rigby/components/microInteractions/LoadingBar';
//Context
import {GeneralConfigContext, UserContext} from '@rigby/components/context';
//Variations
import {MainHeader} from './MainHeader/MainHeader.component';
import {ProfileHeader} from './ProfileHeader/ProfileHeader.component';
/**
 * The header component of the app, their variant corresponds to the app flows
 * Contains 2 variants: Main and Profile
 * @param {*} props
 */
export const Header = (props) => {
  //Global state
  const user = useContext(UserContext);
  const appConfig = useContext(GeneralConfigContext);
  /**
   * Handles the press on the profile image while in the main flow
   */
  function handleProfilePress() {
    appConfig.appNavigation(appConfig.navigation, 'ProfileFlow', {
      screen: 'Profile',
      params: {userId: user.userId},
    });
  }
  /**
   * Handles the back button press on the profile flow
   */
  function handleBackPress() {
    appConfig.appGoBack(appConfig.navigation);
  }

  return (
    <SafeAreaView style={styles.safeAreaWrapper}>
      <View
        style={styles.innerWrapper}>
        <View style={styles.header}>
          <MainHeader
            handleProfilePress={() => handleProfilePress()}
            visible={user.isLogged && appConfig.currentScreen !== 'Profile'}
          />
          <ProfileHeader
            visible={appConfig.currentScreen === 'Profile'}
            onPressBack={handleBackPress}
          />
          {appConfig.loading ? <LoadingBar /> : null}
        </View>
        {user.isLogged && appConfig.currentScreen !== 'Profile' && (
          <LinearGradient
            colors={gradients.gradient2Inverted}
            style={styles.bottomGradient}
          />
        )}
      </View>
    </SafeAreaView>
  );
};
Header.propTypes = {
  ...model.types,
};

Header.defaultProps = {
  ...model.default,
};
