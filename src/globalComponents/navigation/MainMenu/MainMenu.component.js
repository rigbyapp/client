import React, {useState, useRef, useContext} from 'react';
import {View, Animated, Easing} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {PanGestureHandler, State} from 'react-native-gesture-handler';
//Rigby
import {gradients} from '@rigby/theme/colors.style';
import {SlideInView} from '@rigby/components/microInteractions/SlideInView';
import {MainMenuTab} from '@rigby/components/navigation/MainMenuTab';
import {GeneralConfigContext} from '@rigby/components/context';
//style
import {styles} from './MainMenu.style';

//Sections
import * as DATA from '@rigby/data/MAIN_MENU_SECTIONS.json';

//Made an inner component since React navigation bugs out when using hooks :I
export const InnerMainMenu = (props) => {
  const [menuItemWidths, setMenuItemWidths] = useState(DATA.menuSections);
  const menuOffsetX = useRef(new Animated.Value(0)).current;
  const appConfig = useContext(GeneralConfigContext);
  //Slide animation
  const duration = 800;
  const slideAnim = useRef(new Animated.Value(0)).current;
  const barGrowAnim = useRef(new Animated.Value(0)).current;
  const barPositionAnim = useRef(new Animated.Value(0)).current; // Initial value for opacity: 0
  //Animation of the bottom Bar
  function animateBottomBar(index) {
    if (menuItemWidths[index].width) {
      Animated.timing(barGrowAnim, {
        toValue: menuItemWidths[index].width / 100,
        duration: duration,
        useNativeDriver: true,
        easing: Easing.bezier(0.19, 1.0, 0.22, 1.0),
      }).start();
      Animated.timing(barPositionAnim, {
        toValue: (menuItemWidths[index].width - 100) * 0.25,
        duration: duration,
        useNativeDriver: true,
        easing: Easing.bezier(0.19, 1.0, 0.22, 1.0),
      }).start();
    }
  }
  //Animation of the reel of menu options
  function animateMenuTitles(index) {
    var distance = menuItemWidths[index].distance
      ? menuItemWidths[index].distance
      : 0;

    Animated.timing(menuOffsetX, {
      toValue: -distance,
      duration: duration,
      useNativeDriver: true,
      easing: Easing.bezier(0.19, 1.0, 0.22, 1.0),
    }).start();
    //Gives width to the bottomBar once the menuItems widths are populated from the onLayout prop
  }
  //When we load the menu options, we get the width of each option and save them in the state
  function updateMenuWidths(eventLayout, index, name) {
    menuItemWidths[index].distance = eventLayout.x;
    menuItemWidths[index].width = eventLayout.width;
    menuItemWidths[index].key = index;
    setMenuItemWidths(menuItemWidths);
    animateMenuTitles(props.state.index);
    animateBottomBar(props.state.index);
  }

  //Once a menu option is clicked or swiped we get the screen where it wants to go and the index o said option on the array
  function updateMenuPosition(screen, index) {
    appConfig.appNavigation(props.navigation, 'MainFlow', {
      screen: screen.toScreen,
    });
    animateMenuTitles(index);
    animateBottomBar(index);
  }

  //This handles the end of a swipe(when the user releases the tap)
  function handleRegisteredSwipe(eventState) {
    //Checks that the gesture is completed
    if (eventState.oldState === State.ACTIVE) {
      const direction = eventState.translationX < 0 ? 1 : -1;
      //Checks that there is an object on that item of the array
      var menuItem =
        props.state.index + direction >= 0 && props.state.index + direction <= 3
          ? props.state.index + direction
          : props.state.index;
      //Checks that the distance (30pts) was drag
      if (eventState.translationX * direction < -30) {
        updateMenuPosition(menuItemWidths[menuItem], menuItem);
      } else {
        //if not the screen does not change
        updateMenuPosition(
          menuItemWidths[props.state.index],
          props.state.index,
        );
      }
    }
  }
  //Handles the drag while its happening
  function handleSemiSwipe(eventState) {
    menuOffsetX.setValue(
      -menuItemWidths[props.state.index].distance + eventState.translationX,
    );
  }

  return (
    <>
      <LinearGradient colors={gradients.gradient2} style={styles.topGradient} />
      <PanGestureHandler
        onHandlerStateChange={({nativeEvent}) =>
          handleRegisteredSwipe(nativeEvent)
        }
        onGestureEvent={({nativeEvent}) => handleSemiSwipe(nativeEvent)}
        minDist={1}>
        <LinearGradient
          colors={gradients.gradientMainMenu}
          style={styles.menuWrapper}>
          <SlideInView.Right distance={500} style={{...styles.reel}}>
            <Animated.View
              style={{
                transform: [{translateX: menuOffsetX}],
              }}>
              <View
                style={{
                  ...styles.textWrapper,
                  transform: [{translateX: 0}],
                }}>
                {DATA.menuSections.map(function (section, i) {
                  return (
                    <MainMenuTab
                      section={section.name}
                      stateScreenId={props.state.index}
                      key={i}
                      screenId={i}
                      onPress={() => updateMenuPosition(section, i)}
                      onLayout={(event) =>
                        updateMenuWidths(
                          event.nativeEvent.layout,
                          i,
                          section.name,
                        )
                      }
                    />
                  );
                })}
              </View>
            </Animated.View>
            <Animated.View
              style={{
                ...styles.bottomGradientWrapper,
                transform: [
                  {scaleX: barGrowAnim},
                  {translateX: barPositionAnim},
                ],
              }}>
              <LinearGradient
                start={{x: 0, y: 0}}
                end={{x: 1, y: 0}}
                colors={gradients.gradient1}
                style={{
                  ...styles.bottomGradient,
                }}
              />
            </Animated.View>
          </SlideInView.Right>
        </LinearGradient>
      </PanGestureHandler>
    </>
  );
};

export const MainMenu = (props) => {
  return <InnerMainMenu {...props} />;
};
