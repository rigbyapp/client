import {StyleSheet} from 'react-native';
import {colors} from '@rigby/theme/colors.style';
import {layout} from '@rigby/theme/layout.style';

export const styles = StyleSheet.create({
  menuWrapper: {
    height: 81 + layout.bottomAreaSize,
    backgroundColor: colors.background,
  },
  reel: {
    paddingTop: 20,
    //backgroundColor:'yellow',
  },
  textWrapper: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    justifyContent: 'center',
    //backgroundColor:'blue'
  },
  topGradient: {
    position: 'absolute',
    bottom: 81 + layout.bottomAreaSize,
    height: 30,
    width: '100%',
    zIndex: 4,
  },
  bottomGradientWrapper: {paddingLeft: 20, width: 100},
  bottomGradient: {
    marginTop: 5,
    height: 5,
    borderRadius: 50,
    width: '100%',
  },
});
