import React from 'react';
import {View, TouchableWithoutFeedback} from 'react-native';
import {styles} from './MainMenuTab.style';
import {model} from './MainMenuTab.model';
import {Typography} from '@rigby/components/text/Typography';

export const MainMenuTab = (props) => {
  return (
    <TouchableWithoutFeedback onPress={props.onPress} onLayout={props.onLayout}>
      <View style={styles.tabWrapper}>
        <Typography
          variant={'subtitle'}
          color={props.stateScreenId === props.screenId ? 'text1' : 'text3'}>
          {props.section}
        </Typography>
      </View>
    </TouchableWithoutFeedback>
  );
};
MainMenuTab.propTypes = {
  ...model.types,
};

MainMenuTab.defaultProps = {
  ...model.default,
};
