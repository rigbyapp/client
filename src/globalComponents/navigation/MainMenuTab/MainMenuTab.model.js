/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    section: PropTypes.string.isRequired,
    active: PropTypes.bool.isRequired,
    onPress: PropTypes.func.isRequired,
  },
  default: {
    section: 'SAMPLE SECTION',
    active: false,
    onPress: () => console.log('Tab Press Check'),
  },
};
