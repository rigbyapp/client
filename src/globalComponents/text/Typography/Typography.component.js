import React from 'react';
import {Text} from 'react-native';
//Style
import {styles} from './Typography.style';
//Model
import {model} from './Typography.model';
//Rigby
import {colors} from '@rigby/theme/colors.style';
import {typography} from '@rigby/theme/typography.style';
import {Skeleton} from '@rigby/components/microInteractions/Skeleton';
/**
 * The base text component, see the text styles of the theme for more info.
 * @param {*} props
 */
export const Typography = (props) => {
  const styleApplied = typography[props.variant];

  return !props.forceLoadState ? (
    <Text
      style={{
        ...styleApplied,
        ...props.style,
        textTransform: props.textTransform,
        color: colors[props.color],
        textAlign: props.align,
      }}>
      {props.children}
    </Text>
  ) : (
    <Skeleton
      width={props.skeletonWidth}
      height={styleApplied.fontSize}
      borderRadius={100}
      style={styles.skeleton}
    />
  );
};
Typography.propTypes = {
  ...model.types,
};

Typography.defaultProps = {
  ...model.default,
};
