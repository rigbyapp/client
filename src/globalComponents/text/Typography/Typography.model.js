/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    children: PropTypes.node.isRequired,
    variant: PropTypes.string.isRequired,
    textTransform: PropTypes.oneOf([
      'none',
      'uppercase',
      'lowercase',
      'capitalize',
    ]).isRequired,
    color: PropTypes.string.isRequired,
    align: PropTypes.oneOf(['center', 'left', 'right']).isRequired,
    forceLoadState: PropTypes.bool.isRequired,
    skeletonWidth: PropTypes.number.isRequired,
  },
  default: {
    textTransform: 'none',
    children: 'SAMPLE TEXT',
    variant: 'button',
    color: 'text1',
    align: 'left',
    forceLoadState: false,
    skeletonWidth: 40,
  },
};
