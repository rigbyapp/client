import {StyleSheet} from 'react-native';
import {colors} from '@rigby/theme/colors.style';
import {layout} from '@rigby/theme/layout.style';

export const styles = StyleSheet.create({
  gigaTitle: {
    position: 'absolute',
    width: 1200,
    left: -10,
    transform: [{translateY: -70}],
  },
  gigaTitleWrapper: {
    marginBottom: 25,
  },
  bottomGradient: {
    height: 25,
    width: '100%',
    transform: [{translateY: 30}],
    position: 'absolute',
  },
});
