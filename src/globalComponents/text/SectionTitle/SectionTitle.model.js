/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    children: PropTypes.string.isRequired,
  },
  default: {
    children: 'SAMPLE TITLE',
  },
};
