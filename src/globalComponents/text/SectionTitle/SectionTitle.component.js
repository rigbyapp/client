import React from 'react';
import {View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
//Style
import {styles} from './SectionTitle.style';
//Model
import {model} from './SectionTitle.model';
//Rigby
import {Typography} from '@rigby/components/text/Typography';
import {SlideInView} from '@rigby/components/microInteractions/SlideInView';
import {Button} from '@rigby/components/inputs/Button';
import {gradients} from '@rigby/theme/colors.style';
/**
 * A giant title that slides in from the right mainly used for section titles
 * @param {*} props
 */
export const GigaTitle = (props) => {
  return (
    <View style={styles.gigaTitleWrapper}>
      <View style={styles.gigaTitle}>
        <SlideInView.Right duration={1600}>
          <Typography variant={'giga'} color={'disabled'}>
            {`${props.children}.`}
          </Typography>
        </SlideInView.Right>
      </View>
      <SlideInView.Right style={styles.titleInnerWrapper}>
        {props.onPressBack ? (
          <Button.Icon onPress={props.onPressBack} style={styles.backButton} />
        ) : null}
        <Typography variant="title">{`${props.children}.`}</Typography>
      </SlideInView.Right>
    </View>
  );
};
/**
 * A smaller that slides in from the right mainly used for section titles
 * @param {*} props
 */
export const MiniTitle = (props) => {
  return (
    <View style={{...styles.titleInnerWrapper, ...styles.miniTitleWrapper}}>
      <SlideInView.Right>
        <Typography variant="subtitle3" align="center">
          {props.children}
        </Typography>
      </SlideInView.Right>
      <LinearGradient
        colors={gradients.gradient2Inverted}
        style={styles.bottomGradient}
      />
    </View>
  );
};

MiniTitle.propTypes = {
  ...model.types,
};

MiniTitle.defaultProps = {
  ...model.default,
};
GigaTitle.propTypes = {
  ...model.types,
};

GigaTitle.defaultProps = {
  ...model.default,
};
