/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    visible: PropTypes.bool.isRequired,
    component: PropTypes.node,
  },
  default: {
    visible: false,
  },
};
