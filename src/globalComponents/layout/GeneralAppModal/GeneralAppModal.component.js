import React, {useContext} from 'react';
import {View} from 'react-native';
//Style
import {styles} from './GeneralAppModal.style';
//Model
import {model} from './GeneralAppModal.model';
//Rigby
import {PopModal} from '@rigby/components/layout/PopModal';
import {colors} from '@rigby/theme/colors.style';
//Context
import {GeneralConfigContext} from '@rigby/components/context';
/**
 * A global modal that can be ppoped up anywhere on the app, mainly used for displaying posts.
 * You need to pass a component to display
 * @param {*} props
 */
export const GeneralAppModal = (props) => {
  const appConfig = useContext(GeneralConfigContext);
  return (
    <PopModal
      visible={appConfig.generalModal}
      backgroundColor={colors.background}>
      <View style={styles.contentWrapper}>
        {appConfig.generalModalComponent}
      </View>
    </PopModal>
  );
};
GeneralAppModal.propTypes = {
  ...model.types,
};

GeneralAppModal.defaultProps = {
  ...model.default,
};
