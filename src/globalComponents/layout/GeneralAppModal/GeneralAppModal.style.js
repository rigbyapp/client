import {StyleSheet, Dimensions} from 'react-native';
import {layout} from '@rigby/theme/layout.style';

const DEVICE_HEIGHT = Dimensions.get('window').height;

export const styles = StyleSheet.create({
  contentWrapper: {
    top: 0,
    minHeight: DEVICE_HEIGHT - layout.topAreaSize,
    backgroundColor: 'white',
  },
});
