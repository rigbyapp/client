/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    title: PropTypes.string.isRequired,
    onPressBack: PropTypes.func.isRequired,
    children: PropTypes.node,
  },
  default: {
    title: 'EXAMPLE_SECTION',
    onPressBack: () => {
      console.log('Back on section screen');
    },
  },
};
