import React, {useState, useRef, useEffect} from 'react';
import {View, Animated, Easing} from 'react-native';
//Style
import {styles} from './SectionScreen.style';
//Model
import {model} from './SectionScreen.model';
//Rigby
import {Button} from '@rigby/components/inputs/Button';
import {GigaTitle, MiniTitle} from '@rigby/components/text/SectionTitle';
import {ScrollView} from 'react-native-gesture-handler';
/**
 * A screen preconfigured to show a title at the top of it mainly for sections
 * @param {*} props
 */
export const SectionScreen = (props) => {
  const [scrollOffset, setScrollOffset] = useState(0);
  const fadeAnim = useRef(new Animated.Value(0)).current;
  useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: scrollOffset / 30,
      duration: 1,
      useNativeDriver: true,
      easing: Easing.in,
    }).start();
  }, [fadeAnim, scrollOffset]);
  //console.log(scrollOffset);
  return (
    <>
      <Animated.View style={{...styles.miniTitleWrapper, opacity: fadeAnim}}>
        <MiniTitle>{props.title}</MiniTitle>
      </Animated.View>
      {props.onPressBack ? (
        <View style={styles.backButton}>
          <Button.Icon onPress={props.onPressBack} />
        </View>
      ) : null}
      <ScrollView
        style={styles.innerWrapper}
        scrollEventThrottle={10}
        onScroll={(e) => setScrollOffset(e.nativeEvent.contentOffset.y)}>
        <View style={styles.content}>
          <Animated.View
            style={{
              opacity: fadeAnim.interpolate({
                inputRange: [0, 1],
                outputRange: [1, 0],
              }),
            }}>
            <GigaTitle>{props.title}</GigaTitle>
          </Animated.View>
          {props.children}
        </View>
      </ScrollView>
    </>
  );
};
SectionScreen.propTypes = {
  ...model.types,
};

SectionScreen.defaultProps = {
  ...model.default,
};
