import {StyleSheet} from 'react-native';
import {colors} from '@rigby/theme/colors.style';

export const styles = StyleSheet.create({
  innerWrapper: {
    paddingHorizontal: 20,
  },
  backButton: {
    position: 'absolute',
    zIndex: 5,
    paddingLeft: 5,
    height: 40,
    justifyContent: 'center',
  },
  miniTitleWrapper: {
    justifyContent: 'center',
    position: 'absolute',
    width: '100%',
    backgroundColor: colors.background,
    zIndex: 2,
    paddingVertical: 10,
  },
  content: {
    paddingTop: 50,
    paddingBottom: 20,
  },
});
