import {Profile} from './Profile/Profile.component';
/**
 * The card component
 * Consists of 1 variant: Profile
 */
export const Card = {
  Profile,
};
