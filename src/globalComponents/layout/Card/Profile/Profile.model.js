/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    image: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    age: PropTypes.number.isRequired,
    onPress: PropTypes.func.isRequired,
  },
  default: {
    name: 'NAME',
    lastName: 'LASTNAME',
    age: 18,
    onPress: () => {
      console.log('Profile card press');
    },
  },
};
