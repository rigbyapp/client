import {StyleSheet, useWindowDimensions} from 'react-native';

export const styles = StyleSheet.create({
  imageWrapper: {
    marginHorizontal: 20,
  },
  image: {
    borderRadius: 5,
    marginBottom: 10,
  },
});
