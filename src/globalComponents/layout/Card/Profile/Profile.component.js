import React from 'react';
import {View, useWindowDimensions, TouchableOpacity} from 'react-native';
//Style
import {styles} from './Profile.style';
//Model
import {model} from './Profile.model';
//Rigby
import {Typography} from '@rigby/components/text/Typography';
import {BaseImage} from '@rigby/components/media/BaseImage';
/**
 * The base profle card component mainly used on the Meet screen
 * @param {*} props
 */
export const Profile = (props) => {
  const windowWidth = useWindowDimensions().width;

  function handlePress() {
    props.onPress();
  }

  return (
    <TouchableOpacity
      style={styles.imageWrapper}
      onPress={() => {
        handlePress();
      }}>
      <BaseImage
        source={{uri: props.image}}
        style={{
          ...styles.image,
        }}
        width={windowWidth / 1.6}
        height={windowWidth / 1.6}
      />
      <View>
        <Typography
          variant={'subtitle3'}>{`${props.name} ${props.lastName}`}</Typography>
        <Typography variant={'subtitle3Regular'}>{props.age}</Typography>
        <Typography>Fotógrafa</Typography>
      </View>
    </TouchableOpacity>
  );
};
Profile.propTypes = {
  ...model.types,
};

Profile.defaultProps = {
  ...model.default,
};
