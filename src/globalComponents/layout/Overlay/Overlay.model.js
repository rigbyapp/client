/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    zIndex: PropTypes.number.isRequired,
    onPress: PropTypes.func,
  },
  default: {
    zIndex: 10,
    onPress: null,
  },
};
