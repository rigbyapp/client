import React from 'react';
import {TouchableOpacity} from 'react-native';
//Model
import {model} from './Overlay.model';
//Style
import {styles} from './Overlay.style';
//Rigby
import {FadeInView} from '@rigby/components/microInteractions/FadeInView';
/**
 * An overlay appears infront of the whole app with the exception of modals
 * @param {*} props
 */
export const Overlay = (props) => {
  return (
    <FadeInView
      style={{...styles.overlayWrapper, zIndex: props.zIndex}}
      duration={200}>
      <TouchableOpacity
        style={styles.overlay}
        onPress={props.onPress}
        disabled={!props.onPress}
      />
    </FadeInView>
  );
};

Overlay.propTypes = {
  ...model.types,
};

Overlay.defaultProps = {
  ...model.default,
};
