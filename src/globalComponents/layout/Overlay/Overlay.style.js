import {StyleSheet} from 'react-native';
import {colors} from '@rigby/theme/colors.style';
export const styles = StyleSheet.create({
  overlayWrapper: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    zIndex: 10,
  },
  overlay: {
    backgroundColor: colors.background,
    opacity: 0.6,
    width: '100%',
    height: '100%',
  },
});
