import React, {useState, useEffect} from 'react';
import {View, Modal} from 'react-native';
//Style
import {styles} from './PopModal.style';
//Model
import {model} from './PopModal.model';
//Rigby
import {Overlay} from '@rigby/components/layout/Overlay';
/**
 * A pop up modal, it takes the height of its container
 * @param {*} props
 */
export const PopModal = (props) => {
  //Local state
  const [modalVisible, setModalVisible] = useState(props.visible);

  useEffect(() => {
    setModalVisible(props.visible);
  }, [props.visible]);

  return (
    <Modal animationType={'slide'} transparent={true} visible={modalVisible}>
      <Overlay zIndex={0} onPress={props.onPressOut} />
      <View
        style={{...styles.modalView, backgroundColor: props.backgroundColor}}>
        {props.children}
      </View>
    </Modal>
  );
};
PopModal.propTypes = {
  ...model.types,
};

PopModal.defaultProps = {
  ...model.default,
};
