/********** Model of this component ************/
import PropTypes from 'prop-types';
import {colors} from '@rigby/theme/colors.style';

export const model = {
  types: {
    backgroundColor: PropTypes.string.isRequired,
    visible: PropTypes.bool.isRequired,
    onPressOut: PropTypes.func,
  },
  default: {
    backgroundColor: colors.inputBackground,
    visible: true,
    onPressOut: null,
  },
};
