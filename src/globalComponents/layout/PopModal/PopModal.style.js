import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  modalView: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
  },
});
