import {StyleSheet, Dimensions} from 'react-native';
import {layout} from '@rigby/utils/theme/layout.style';

export const styles = StyleSheet.create({
  postWrapper: {
    minHeight: '100%',
  },
  userData: {
    flexDirection: 'row',
    position: 'absolute',
    top: layout.topAreaSize,
    marginHorizontal: 15,
    alignItems: 'center',
  },
  profilePicture: {
    marginVertical: 10,
    marginRight: 10,
  },
  background: {
    position: 'absolute',
  },
  gradient: {
    position: 'absolute',
    height: layout.topAreaSize + 120,
    width: '100%',
  },
});
