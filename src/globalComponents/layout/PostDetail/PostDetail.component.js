import React, {useEffect, useContext, useState} from 'react';
import {View, useWindowDimensions, SafeAreaView} from 'react-native';
// Style
import {styles} from './PostDetail.style';
//Model
import {model} from './PostDetail.model';
//Rigby
import {Typography} from '@rigby/components/text/Typography';
import {BaseImage} from '@rigby/components/media/BaseImage';
import {gradients} from '@rigby/theme/colors.style';
import {fromNow} from '@rigby/utils/commons/format/fromNow.js';
//Context
import {GeneralConfigContext, UserContext} from '@rigby/components/context';
import LinearGradient from 'react-native-linear-gradient';
import {PanGestureHandler} from 'react-native-gesture-handler';
/**
 * The base post component
 * @param {*} props
 */
export const PostDetail = (props) => {
  const moment = require('moment');
  //Global state
  const user = useContext(UserContext);
  const appConfig = useContext(GeneralConfigContext);
  //Local state
  const DIMENSIONS = useWindowDimensions();
  const [profileData, setProfileData] = useState({});
  //Lifecycle
  useEffect(() => {
    props.fetchProfileData(user, props.post.userId, appConfig, setProfileData);
  }, [props.post.userId]);

  function handleSwipe(eventState) {
    const direction = eventState.translationY <= 0 ? 1 : -1;
    console.log(direction);
    direction === -1 && appConfig.setGeneralModal(false);
  }

  return (
    <PanGestureHandler
      onHandlerStateChange={({nativeEvent}) => {
        handleSwipe(nativeEvent);
      }}
      minDist={80}>
      <View style={styles.postWrapper}>
        <BaseImage
          height={DIMENSIONS.height}
          width={DIMENSIONS.width}
          source={{
            uri: props.post.thumbnail,
          }}
          blurRadius={50}
          style={styles.background}
        />
        <LinearGradient colors={gradients.gradient4} style={styles.gradient} />
        <SafeAreaView style={styles.userData}>
          <BaseImage
            width={35}
            height={35}
            source={{uri: profileData.profilePicture}}
            style={styles.profilePicture}
            forceLoadState={appConfig.loading}
          />
          <View>
            <Typography
              forceLoadState={appConfig.loading}
              variant={'bold'}
              skeletonWidth={
                150
              }>{`${profileData.firstName} ${profileData.lastName}`}</Typography>
            <Typography forceLoadState={appConfig.loading} variant={'caption'}>
              {fromNow(props.post.timestamp, appConfig.copies.dateLocale)}
            </Typography>
          </View>
        </SafeAreaView>
        <BaseImage
          height={DIMENSIONS.height}
          width={DIMENSIONS.width}
          resizeMode={'contain'}
          source={{
            uri: props.post.thumbnail,
          }}
        />
      </View>
    </PanGestureHandler>
  );
};
PostDetail.propTypes = {
  ...model.types,
};

PostDetail.defaultProps = {
  ...model.default,
};
