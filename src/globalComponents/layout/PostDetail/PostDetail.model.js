/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    post: PropTypes.object.isRequired,
  },
  default: {},
};
