import React from 'react';
import {View, TouchableOpacity} from 'react-native';
//Style
import {styles} from './Friend.style';
//Model
import {model} from './Friend.model';
//Rigby
import {Typography} from '@rigby/components/text/Typography';
import {BaseImage} from '@rigby/components/media/BaseImage';
/**
 * A list item for displaying users mainly used in the Friends screen
 * @param {*} props
 */
export const Friend = (props) => {
  return (
    <TouchableOpacity style={styles.listItem} onPress={props.onPress}>
      <View style={styles.listItemInner}>
        <BaseImage
          width={60}
          height={60}
          style={styles.profilePhoto}
          source={{uri: props.image}}
        />
        <View style={styles.briefWrapper}>
          <Typography variant={'subtitle3Regular'}>
            {`${props.name} ${props.lastName} `}
          </Typography>
        </View>
      </View>
    </TouchableOpacity>
  );
};
Friend.propTypes = {
  ...model.types,
};

Friend.defaultProps = {
  ...model.default,
};
