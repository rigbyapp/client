/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    name: PropTypes.string.isRequired,
    image: PropTypes.oneOfType([PropTypes.object, PropTypes.string]).isRequired,
    onPress: PropTypes.func.isRequired,
  },
  default: {
    name: 'SAMPLE NAME',
    onPress: () => {
      console.log('PRESS CHECK');
    },
  },
};
