import {Notification} from './Notification/Notification.component';
import {Friend} from './Friend/Friend.component';
/**
 * The main component for flat lists
 * Contains 2 variants: Message and Friend
 */
export const ListItem = {
  Notification,
  Friend,
};
