import {StyleSheet} from 'react-native';
import {colors} from '@rigby/theme/colors.style';

export const styles = StyleSheet.create({
  listItem: {
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderBottomColor: colors.border1,
    borderTopColor: colors.border1,
    paddingHorizontal: 15,
    paddingVertical: 25,
  },
  listItemInner: {
    flexDirection: 'row',
  },
  profilePhoto: {
    backgroundColor: colors.secondary,
    marginRight: 20,
  },
  date: {
    paddingTop: 5,
    height: 20,
    alignItems: 'flex-end',
  },
  quoteWrapper: {
    marginVertical: 5,
  },
  quote: {
    width: 200,
  },
  briefWrapper: {
    justifyContent: 'center',
  },
});
