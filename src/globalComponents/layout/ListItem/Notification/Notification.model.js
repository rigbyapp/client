/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    image: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
    senderId: PropTypes.number.isRequired,
  },
  default: {},
};
