import React from 'react';
import {Touchable, TouchableOpacity, View} from 'react-native';
//Style
import {styles} from './Notification.style';
//Model
import {model} from './Notification.model';
//Rigby
import {Typography} from '@rigby/components/text/Typography';
import {BaseImage} from '@rigby/components/media/BaseImage';
/**
 * A list item for displaying message notifications on the Home screen
 * @param {*} props
 */
export const Notification = (props) => {
  return (
    <View style={styles.listItem}>
      <View style={styles.listItemInner}>
        <TouchableOpacity onPress={props.onPress}>
          <BaseImage
            width={60}
            height={60}
            borderRadius={5}
            style={styles.profilePhoto}
            source={{uri: props.image}}
          />
        </TouchableOpacity>
        <View style={styles.briefWrapper}>
          <Typography variant={'caption'}>
            {`${props.firstName} ${props.lastName} `}
            <Typography variant={'caption'} color={'text2'}>
              te ha respondido.
            </Typography>
          </Typography>
          {props.description.length > 0 && (
            <View style={styles.quoteWrapper}>
              <Typography variant={'quote'} style={styles.quote}>
                {props.description}
              </Typography>
            </View>
          )}
          <Typography variant={'caption'} color={'text2'}>
            {props.time}
          </Typography>
        </View>
      </View>
    </View>
  );
};
Notification.propTypes = {
  ...model.types,
};

Notification.defaultProps = {
  ...model.default,
};
