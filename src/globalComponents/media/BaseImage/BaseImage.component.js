import React, {useState, useEffect} from 'react';
import {View, Image} from 'react-native';
//Style
import {styles} from './BaseImage.style';
//Model
import {model} from './BaseImage.model';
//Rigby
import {Skeleton} from '@rigby/components/microInteractions/Skeleton';
import placeholderDefault from '@rigby/assets/placeholders/ry_brokenLink.png';
/**
 * Base image component
 * @param {*} props
 */
export const BaseImage = (props) => {
  const [src, setSrc] = useState(props.source);
  const [loading, setLoading] = useState(true);
  /**
   * Handles the end of the image loading wether it failed or not
   */
  function handleOnLoadEnd() {
    setLoading(false);
  }
  var finalPlaceholder = props.placeholderImg
    ? props.placeholderImg
    : placeholderDefault;
  /**
   * Returns props for the component
   * @param {*} source
   * @param {*} fallback
   * @return {object} The source(either the image or a fallback in case of an error) and the error handler
   */
  function useFallbackImg(source, fallback) {
    /**
     * Handles image's errors
     * @param {*} e The error object
     */
    function onError(e) {
      // React bails out of hook renders if the state
      // is the same as the previous state, otherwise
      // fallback erroring out would cause an infinite loop
      setSrc(fallback);
      console.log(e);
    }

    return {source, onError};
  }
  const srcProps = useFallbackImg(src, finalPlaceholder);
  useEffect(() => {
    setSrc(props.source);
  }, [props.source]);
  return (
    <>
      <Image
        onLoadEnd={handleOnLoadEnd}
        style={{
          //Yeah I know there's a ton of stuff inline
          ...styles.base,
          opacity: props.forceLoadState || loading ? 0 : 1,
          width: props.width,
          height: props.height,
          resizeMode: props.resizeMode,
          borderRadius: props.borderRadius,
          ...props.style,
        }}
        blurRadius={props.blurRadius}
        {...srcProps}
      />
      {(props.forceLoadState || loading) && (
        <Skeleton
          width={props.width}
          height={props.height}
          style={styles.skeleton}
          borderRadius={props.borderRadius}
        />
      )}
    </>
  );
};
BaseImage.propTypes = {
  ...model.types,
};

BaseImage.defaultProps = {
  ...model.default,
};
