/********** Model of this component ************/
import placeholderDefault from '@rigby/assets/placeholders/ry_brokenLink.png';
import PropTypes from 'prop-types';

export const model = {
  types: {
    source: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    placeholderImg: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    resizeMode: PropTypes.string.isRequired,
    forceLoadState: PropTypes.bool.isRequired,
    borderRadius: PropTypes.number.isRequired,
    blurRadius: PropTypes.number.isRequired,
  },
  default: {
    source: placeholderDefault,
    width: 30,
    height: 30,
    resizeMode: 'cover',
    forceLoadState: false,
    borderRadius: 5,
    blurRadius: 0,
  },
};
