/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    onPress: PropTypes.func.isRequired,
    description: PropTypes.string,
  },
  default: {
    onPress: () => {
      console.log('CHECKBOX PRESSED');
    },
    description: 'PLACEHOLDER FOR CHECKBOX',
  },
};
