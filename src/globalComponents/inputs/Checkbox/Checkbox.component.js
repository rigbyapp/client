import React, {useState} from 'react';
import {View, TouchableOpacity} from 'react-native';
//Icons
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
//Style
import {styles} from './Checkbox.style';
//Model
import {model} from './Checkbox.model';
//Rigby
import {Typography} from '@rigby/components/text/Typography';
import {colors} from '@rigby/theme/colors.style';
/**
 * A simple checkbox
 * @param {*} props
 */
export const Checkbox = (props) => {
  const [checked, setChecked] = useState(false);

  function handlePress(onPress) {
    onPress();
    setChecked(!checked);
  }
  return (
    <TouchableOpacity
      style={styles.touchableWrapper}
      onPress={() => handlePress(props.onPress)}>
      <View style={styles.checkBoxwrapper}>
        {checked && (
          <FontAwesomeIcon
            icon={'check-square'}
            size={20}
            color={colors.text1}
          />
        )}
      </View>
      <Typography>{props.description}</Typography>
    </TouchableOpacity>
  );
};
Checkbox.propTypes = {
  ...model.types,
};

Checkbox.defaultProps = {
  ...model.default,
};
