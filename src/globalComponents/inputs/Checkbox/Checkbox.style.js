import {StyleSheet} from 'react-native';
import {colors} from '@rigby/theme/colors.style';

export const styles = StyleSheet.create({
  touchableWrapper: {
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  checkBoxwrapper: {
    marginRight: 10,
    backgroundColor: colors.inputBackground,
    width: 20,
    height: 20,
  },
});
