import React, {useRef, useEffect} from 'react';
import {TouchableOpacity, Animated, Easing} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
//Style
import {styles} from './Button.style';
//Model
import {model} from './Button.model';
//Rigby
import {Typography} from '@rigby/components/text/Typography';
import {gradients} from '@rigby/theme/colors.style';
import {Skeleton} from '@rigby/components/microInteractions/Skeleton';

/**
 * The Gradient Variant of the text button
 * @param {*} props
 */
export const Gradient = (props) => {
  var fadeAnim = useRef(new Animated.Value(props.disabled ? 0 : 0.8)).current; // Initial value for opacity: 0
  useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: props.disabled ? 0.8 : 0,
      duration: 300,
      useNativeDriver: true,
      easing: Easing.in,
    }).start();
  });

  return !props.forceLoadState ? (
    <TouchableOpacity
      style={{...styles.buttonGradient, width: props.width, ...props.style}}
      onPress={props.onPress}
      disabled={props.disabled}>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={gradients.gradient1}
        style={styles.linearGradient}>
        <Typography variant={'bold'}>{props.children}</Typography>
        <Animated.View
          style={{...styles.disabledBackground, opacity: fadeAnim}}
        />
      </LinearGradient>
    </TouchableOpacity>
  ) : (
    <Skeleton width={props.width} height={44} borderRadius={5} />
  );
};

Gradient.propTypes = {
  ...model.types,
};

Gradient.defaultProps = {
  ...model.default,
};
