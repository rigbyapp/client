import {StyleSheet} from 'react-native';
import {colors} from '@rigby/theme/colors.style';

export const styles = StyleSheet.create({
  buttonGradient: {
    backgroundColor: colors.secondary,
    width: 184,
    height: 44,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    borderRadius: 5,
    marginBottom: 20,
  },
  disabledBackground: {
    backgroundColor: colors.disabled,
    height: '100%',
    width: '100%',
    position: 'absolute',
  },
  disabledGhostBackground: {
    backgroundColor: colors.background,
    height: '100%',
    width: '100%',
    position: 'absolute',
  },
  disabledGhostBorder: {
    borderColor: '#1C1646',
  },
  buttonGradientGhost: {
    borderColor: colors.main,
    borderWidth: 1,
    width: 184,
    height: 44,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    borderRadius: 5,
    backgroundColor: colors.background,
  },
  linearGradient: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    borderRadius: 5,
  },
});
