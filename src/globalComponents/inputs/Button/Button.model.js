/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    children: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
    disabled: PropTypes.bool.isRequired,
    forceLoadState: PropTypes.bool.isRequired,
    width: PropTypes.number.isRequired,
  },
  default: {
    children: 'SAMPLE TEXT',
    onPress: () => console.log('Press Check'),
    disabled: false,
    forceLoadState: false,
    width: 184,
  },
};
