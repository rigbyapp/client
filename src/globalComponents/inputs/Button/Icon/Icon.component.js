import React, {useContext} from 'react';
import {View} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
//Style
import {styles} from './Icon.style';
//Model
import {model as baseModel} from '../Button.model';
import {model} from './Icon.model';
//Rigby
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Typography} from '@rigby/components/text/Typography';
//Context
import {GeneralConfigContext} from '@rigby/components/context';

/**
 * The icon variant of the button component
 * @param {*} props
 */
export const Icon = (props) => {
  //Global state
  const appConfig = useContext(GeneralConfigContext);
  return (
    <TouchableOpacity
      onPress={props.onPress}
      disabled={props.disabled}
      style={props.style}>
      <View style={styles.inner}>
        <FontAwesomeIcon
          icon={props.icon}
          size={props.size}
          color={props.color}
        />
        {props.icon === 'chevron-left' ? (
          <Typography variant={'caption'}>{appConfig.copies.back}</Typography>
        ) : null}
      </View>
    </TouchableOpacity>
  );
};
Icon.propTypes = {
  ...baseModel.types,
  ...model.types,
};

Icon.defaultProps = {
  ...baseModel.default,
  ...model.default,
};
