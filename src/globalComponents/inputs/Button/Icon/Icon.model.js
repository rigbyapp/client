/********** Model of this component ************/
import PropTypes from 'prop-types';
import {colors} from '@rigby/theme/colors.style';

export const model = {
  types: {
    icon: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    size: PropTypes.number.isRequired,
    style: PropTypes.object.isRequired,
  },
  default: {
    icon: 'chevron-left',
    color: colors.text1,
    size: 25,
    style: {},
  },
};
