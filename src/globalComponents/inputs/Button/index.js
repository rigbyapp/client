import {Gradient} from './Button.gradient.component';
import {Ghost} from './Button.ghost.component';
import {Icon} from './Icon/Icon.component';
import {Link} from './Link/Link.component';
/**
 * The main button component of rigby
 * Consists of 4 variants: Gradient, Ghost, Icon and Link
 * @use <Button.Gradient>Login</Button.Gradient>
 */
export const Button = {
  Gradient,
  Ghost,
  Icon,
  Link,
};
