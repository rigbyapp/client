/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    variant: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
  },
  default: {
    variant: 'bold',
    color: 'text1',
    children: 'SAMPLE_LINK',
  },
};
