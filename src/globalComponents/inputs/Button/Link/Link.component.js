import React from 'react';
import {TouchableOpacity} from 'react-native';
//Style
import {styles} from './Link.style';
//Model
import {model} from './Link.model';
import {model as baseModel} from './Link.model';
//Rigby
import {Typography} from '@rigby/components/text/Typography';
import {typography} from '@rigby/theme/typography.style';
import {Skeleton} from '@rigby/components/microInteractions/Skeleton';

/**
 * A component used for displaying links
 * @param {*} props
 */
export const Link = (props) => {
  return !props.forceLoadState ? (
    <TouchableOpacity
      onPress={props.onPress}
      disabled={props.disabled}
      style={{...styles.wrapper, height: typography[props.variant].fontSize}}>
      <Typography variant={props.variant} color={props.color}>
        {props.children}
      </Typography>
    </TouchableOpacity>
  ) : (
    <Skeleton
      width={props.skeletonWidth}
      height={typography[props.variant].fontSize}
      borderRadius={100}
    />
  );
};
Link.propTypes = {
  ...baseModel.types,
  ...model.types,
};

Link.defaultProps = {
  ...baseModel.default,
  ...model.default,
};
