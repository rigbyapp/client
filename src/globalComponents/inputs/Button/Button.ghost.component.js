import React, {useRef, useEffect} from 'react';
import {TouchableOpacity, Animated, Easing} from 'react-native';
//Style
import {styles} from './Button.style';
//Model
import {model} from './Button.model';
//Rigby
import {Typography} from '@rigby/components/text/Typography';
import {Skeleton} from '@rigby/components/microInteractions/Skeleton';
/**
 * The ghost variant of the text button
 * @param {*} props
 */
export const Ghost = (props) => {
  var fadeAnim = useRef(new Animated.Value(props.disabled ? 0 : 0.9)).current; // Initial value for opacity: 0
  useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: props.disabled ? 0.9 : 0,
      duration: 300,
      useNativeDriver: true,
      easing: Easing.in,
    }).start();
  });
  return !props.forceLoadState ? (
    <TouchableOpacity
      style={{
        ...styles.buttonGradientGhost,
        width: props.width,
        ...(props.disabled ? styles.disabledGhostBorder : null),
        ...props.style,
      }}
      onPress={props.onPress}
      disabled={props.disabled}>
      <Typography variant={'bold'}>{props.children}</Typography>
      <Animated.View
        style={{...styles.disabledGhostBackground, opacity: fadeAnim}}
      />
    </TouchableOpacity>
  ) : (
    <Skeleton width={props.width} height={44} borderRadius={5} />
  );
};

Ghost.propTypes = {
  ...model.types,
};

Ghost.defaultProps = {
  ...model.default,
};
