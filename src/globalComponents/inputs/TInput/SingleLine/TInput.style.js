import {StyleSheet} from 'react-native';
import {colors} from '@rigby/theme/colors.style';
import {typography} from '@rigby/theme/typography.style';

export const styles = StyleSheet.create({
  inputWrapper: {
    marginBottom: 10,
  },
  innerWrapper: {
    marginBottom: 10,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  input: {
    color: colors.text1,
    ...typography.button,
    paddingHorizontal: 10,
    paddingVertical: 13,
    flex: 15,
  },
  tooltip: {
    padding: 10,
  },
  errorBadge: {
    //backgroundColor: 'blue',
    paddingHorizontal: 5,
  },
  errorWrapper: {
    marginBottom: 10,
  },
  nonEditableOverlay: {
    opacity: 0.5,
    backgroundColor: colors.background,
    width: '100%',
    height: '100%',
    position: 'absolute',
  },
});
