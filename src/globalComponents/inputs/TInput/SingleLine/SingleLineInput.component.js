import React, {useEffect, useState} from 'react';
import {View, TextInput} from 'react-native';
//Icons
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
//Style
import {styles} from './TInput.style';
//Model
import {model} from './SingleLineInput.model';
//Rigby
import {colors} from '@rigby/theme/colors.style';
import {Typography} from '@rigby/components/text/Typography';
import {validation} from '@rigby/commons/validation';
import {Button} from '@rigby/components/inputs/Button';
/**
 * The base component for single line text input
 * @param {*} props
 */
export const SingleLineInput = (props) => {
  //Local state
  const [actualBorderColor, onChangeBorder] = useState(colors.inputBackground);
  const [error, setError] = useState(false);
  /**
   * Handles the validation of the input
   * @param {String} text
   */
  function handleChangeText(text) {
    props.onChangeText(text);
    onChangeBorder(colors.inputBackgroundFocused);
    if (validation[props.validationType](text, props.countryCode)) {
      setError(false);
    } else {
      setError(true);
    }
  }
  /**
   * Handles the border color when the user end sediting
   */
  function handleEndEditing() {
    onChangeBorder(colors.inputBackground);
  }
  return (
    <View style={{...styles.inputWrapper}}>
      <View
        style={{...styles.innerWrapper, backgroundColor: actualBorderColor}}>
        <TextInput
          onChangeText={(text) => handleChangeText(text)}
          value={props.value}
          style={{...styles.input}}
          placeholder={props.placeHolder}
          placeholderTextColor={colors.text2}
          onEndEditing={() => handleEndEditing()}
          onFocus={() => onChangeBorder(colors.inputBackgroundFocused)}
          secureTextEntry={props.secureTextEntry}
          maxLength={props.maxLength}
          editable={props.editable}
          autoCapitalize={props.autoCapitalize}
          autoCompleteType={props.autoCompleteType}
          keyboardType={props.keyboardType}
        />
        {props.tooltipIcon ? (
          <Button.Icon
            style={styles.tooltip}
            onPress={props.tooltipOnPress}
            icon={props.tooltipIcon}
          />
        ) : null}
        {error ? (
          <View style={styles.errorBadge}>
            <FontAwesomeIcon
              icon={'exclamation-circle'}
              size={25}
              color={colors.error2}
            />
          </View>
        ) : null}
      </View>
      {error ? (
        <View style={styles.errorWrapper}>
          <Typography color={'error2'}>{props.errorMessage}</Typography>
        </View>
      ) : null}
      {!props.editable ? <View style={styles.nonEditableOverlay} /> : null}
    </View>
  );
};
SingleLineInput.propTypes = {
  ...model.types,
};

SingleLineInput.defaultProps = {
  ...model.default,
};
