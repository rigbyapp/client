/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    date: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)])
      .isRequired,
    placeHolder: PropTypes.string.isRequired,
    minDate: PropTypes.instanceOf(Date),
    maxDate: PropTypes.instanceOf(Date),
    editable: PropTypes.bool.isRequired,
    onSelectDate: PropTypes.func.isRequired,
  },
  default: {
    date: new Date(),
    placeHolder: 'DATE INPUT',
    minDate: null,
    maxDate: null,
    editable: true,
    onSelectDate: () => console.log('DATE_SELECTED'),
  },
};
