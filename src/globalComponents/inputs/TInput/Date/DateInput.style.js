import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  pickerWrapper: {
    paddingVertical: 10,
  },
  buttonWrapper: {
    alignItems: 'center',
  },
});
