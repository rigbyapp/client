import React, {useState} from 'react';
import {View} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import {TouchableOpacity} from 'react-native';
//Style
import {styles} from './DateInput.style';
//Model
import {model} from './DateInput.model';
//Rigby
import {PopModal} from '@rigby/components/layout/PopModal/PopModal.component';
import {Button} from '@rigby/components/inputs/Button';
import {styles as inputStyles} from '../SingleLine/TInput.style';
import {colors} from '@rigby/utils/theme/colors.style';
import {Typography} from '@rigby/components/text/Typography';
//moment
import moment from 'moment';
import 'moment/locale/es-mx';

/**
 * A date input
 * The date is saved in ISO strings
 * @param {*} props
 */
export const DateInput = (props) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [date, setDate] = useState(props.date);
  const [inputValue, setInputValue] = useState(props.placeHolder);
  const [textColor, setTextColor] = useState('text2');

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setDate(currentDate);
  };
  function handleSelectDate(dateSelected) {
    let formattedDateForinput = moment(dateSelected)
      .locale(props.locale)
      .format('ll');
    let ISODate = dateSelected.toISOString();
    setInputValue(formattedDateForinput);
    setModalVisible(false);
    props.onSelectDate(ISODate);
  }
  function handleModalShow() {
    setTextColor('text1');
    setModalVisible(true);
  }
  return (
    <>
      <View style={inputStyles.inputWrapper}>
        <TouchableOpacity
          onPress={() => handleModalShow()}
          style={{
            ...inputStyles.innerWrapper,
            backgroundColor: colors.inputBackground,
          }}>
          <View style={inputStyles.input}>
            <Typography color={textColor}>{inputValue}</Typography>
          </View>
        </TouchableOpacity>
        {!props.editable ? (
          <View style={inputStyles.nonEditableOverlay} />
        ) : null}
      </View>
      <PopModal
        backgroundColor={'white'}
        visible={modalVisible}
        onPressOut={() => setModalVisible(false)}>
        <View style={styles.pickerWrapper}>
          <DateTimePicker
            value={date}
            display="spinner"
            onChange={onChange}
            textColor={'blue'}
            maximumDate={props.maxDate}
            minimumDate={props.minDate}
          />
          <View style={styles.buttonWrapper}>
            <Button.Gradient onPress={() => handleSelectDate(date)}>
              Seleccionar
            </Button.Gradient>
          </View>
        </View>
      </PopModal>
    </>
  );
};
DateInput.propTypes = {
  ...model.types,
};

DateInput.defaultProps = {
  ...model.default,
};
