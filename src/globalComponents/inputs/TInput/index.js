import {SingleLineInput} from './SingleLine/SingleLineInput.component';
import {PasswordInput} from './Password/PasswordInput.component';
import {MailInput} from './MailInput/MailInput.component';
import {ZipCodeInput} from './ZipCode/ZipCodeInput.component';
import {DateInput} from './Date/DateInput.component';

export const TInput = {
  Single: SingleLineInput,
  Password: PasswordInput,
  Mail: MailInput,
  ZipCode: ZipCodeInput,
  Date: DateInput,
};
