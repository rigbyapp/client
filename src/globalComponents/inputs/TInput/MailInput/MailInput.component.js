import React from 'react';
//Model
import {model} from './MailInput.model';
//Rigby
import {SingleLineInput} from '../SingleLine/SingleLineInput.component';
/**
 * A single line component pre-configured for mail
 * @param {*} props
 */
export const MailInput = (props) => {
  return (
    <SingleLineInput
      placeHolder={props.placeHolder}
      onChangeText={props.onChangeText}
      value={props.value}
      errorMessage={props.errorMessage}
      validationType={props.validationType}
      editable={props.editable}
      autoCapitalize={'none'}
      autoCompleteType={'email'}
      keyboardType={'email-address'}
    />
  );
};

MailInput.propTypes = {
  ...model.types,
};

MailInput.defaultProps = {
  ...model.default,
};
