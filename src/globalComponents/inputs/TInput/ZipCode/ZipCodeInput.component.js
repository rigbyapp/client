import React from 'react';
//Model
import {model} from './ZipCodeInput.model';
//Rigby
import {SingleLineInput} from '../SingleLine/SingleLineInput.component';

/**
 * A single line component pre-configured for zipcode
 * @param {*} props
 */
export const ZipCodeInput = (props) => {
  return (
    <SingleLineInput
      placeHolder={props.placeHolder}
      onChangeText={props.onChangeText}
      value={props.value}
      errorMessage={props.errorMessage}
      validationType={props.validationType}
      editable={props.editable}
      autoCapitalize={'none'}
      autoCompleteType={'postal-code'}
      countryCode={props.countryCode}
    />
  );
};

ZipCodeInput.propTypes = {
  ...model.types,
};

ZipCodeInput.defaultProps = {
  ...model.default,
};
