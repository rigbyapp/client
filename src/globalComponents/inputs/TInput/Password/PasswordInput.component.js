import React, {useState} from 'react';
import {View} from 'react-native';
//Model
import {model} from './PasswordInput.model';
//Rigby
import {SingleLineInput} from '../SingleLine/SingleLineInput.component';
/**
 * A single line component pre-configured for passwords
 * @param {*} props
 */
export const PasswordInput = (props) => {
  const [visible, onChangeVisible] = useState(true);
  return (
    <View>
      <SingleLineInput
        onChangeText={props.onChangeText}
        value={props.value}
        secureTextEntry={visible}
        placeHolder={props.placeHolder}
        validationType={props.validationType}
        errorMessage={props.errorMessage}
        maxLength={props.maxLength}
        editable={props.editable}
        autoCapitalize={'none'}
        tooltipOnPress={() => onChangeVisible(!visible)}
        tooltipIcon={visible ? 'eye' : 'eye-slash'}
      />
    </View>
  );
};
PasswordInput.propTypes = {
  ...model.types,
};

PasswordInput.defaultProps = {
  ...model.default,
};
