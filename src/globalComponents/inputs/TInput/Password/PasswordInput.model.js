/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    placeHolder: PropTypes.string.isRequired,
    onChangeText: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired,
    validationType: PropTypes.oneOf([
      'email',
      'password',
      'empty',
      'filled',
      'name',
      'none',
    ]),
    errorMessage: PropTypes.string,
    maxLength: PropTypes.number.isRequired,
    editable: PropTypes.bool.isRequired,
  },
  default: {
    placeHolder: 'SAMPLE PASSWORD PLACEHOLDER',
    onChangeText: () => {},
    value: '',
    error: false,
    errorMessage: 'ERR_PASSWORD',
    validationType: 'password',
    maxLength: 50,
    editable: true,
  },
};
