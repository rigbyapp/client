import React, {useState, createContext} from 'react';
import {model} from './UserProvider.model';
//ENV
import {ENV_CONFIG} from '@rigby/config';
/**
 * The global user state uses context to function
 * When using this context please refer to it as 'user'
 * Contains:
 * isLogged - Bool to determine wether a user is logged or not //
 * token - the JWT token //
 * first and last name - Current user's namen //
 * userId - the current user Id //
 * profilePicture - current user's profile picture //
 * birthDate - current user's birth date in ISO string //
 * zipCode - current user's zipcode //
 */
export const UserContext = createContext();
/**
 * The global user state usese context to function contains:
 * isLogged - Bool to determine wether a user is logged or not //
 * token - the JWT token //
 * first and last name - Current user's namen //
 * userId - the current user Id //
 * profilePicture - current user's profile picture //
 * birthDate - current user's birth date in ISO string //
 * zipCode - current user's zipcode //
 * @param {*} props
 */
export const UserProvider = (props) => {
  const [isLogged, setIsLogged] = useState(false);
  const [token, setToken] = useState('');
  const [firstName, setFirstName] = useState('No');
  const [lastName, setLastName] = useState('User');
  const [userId, setUserId] = useState(null);
  const [profilePicture, setProfilePicture] = useState(
    `${ENV_CONFIG.PUBLIC_BUCKET}ry_default.jpg`,
  );
  const [birthDate, setBirthDate] = useState('');
  const [zipcode, setZipcode] = useState('');

  return (
    <UserContext.Provider
      value={{
        isLogged,
        setIsLogged,
        token,
        setToken,
        firstName,
        setFirstName,
        lastName,
        setLastName,
        profilePicture,
        setProfilePicture,
        birthDate,
        setBirthDate,
        zipcode,
        setZipcode,
        userId,
        setUserId,
      }}>
      {props.children}
    </UserContext.Provider>
  );
};
UserProvider.propTypes = {
  ...model.types,
};

UserProvider.defaultProps = {
  ...model.default,
};
