/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    isLogged: PropTypes.bool,
  },
  default: {
    isLogged: false,
  },
};
