import React, {useState, createContext} from 'react';
import {model} from './GeneralConfigProvider.model';
import * as RNLocalize from 'react-native-localize';

/**
 * The global app state/middleware uses context and hooks.
 * When using this context please refer to it as 'appConfig'
 * Contains:
 * // language - The language of the app //
 * theme -  The color scheme of the app //
 * copies - The texts of the app in the language selected //
 * loading - a global loading state //
 * locale - The country code of the device //
 * navigation - A hook to keep track of the navigation state //
 * appNavigation - The main function to navigate through the app //
 * generalModal - A global modal that can always be popped up and passed a component anywhere in the app
 */
export const GeneralConfigContext = createContext();

const copiesGetters = {
  'es-UN': () => require('@rigby/data/languages/es-UN.json'),
  'en-US': () => require('@rigby/data/languages/en-US.json'),
};
/**
 * The global app state/middleware uses context and hooks.
 * Contains:
 * // language - The language of the app //
 * theme -  The color scheme of the app //
 * copies - The texts of the app in the language selected //
 * loading - a global loading state //
 * locale - The country code of the device //
 * navigation - A hook to keep track of the navigation state //
 * appNavigation - The main function to navigate through the app //
 *  * generalModal - A global modal that can always be popped up and passed a component anywhere in the app
 * @param {*} props
 */
export const GeneralConfigProvider = (props) => {
  //State
  const language = RNLocalize.findBestAvailableLanguage(['es-UN', 'en-US']);
  const [theme, setTheme] = useState('dark');
  const [copies, setCopies] = useState(
    copiesGetters[language.languageTag || 'en-US'],
  );
  const [loading, setLoading] = useState(false);
  const [locale, setLocale] = useState(RNLocalize.getCountry());
  const [navigation, setNavigation] = useState('');
  const [currentScreen, setCurrentScreen] = useState('');
  const [history, setHistory] = useState([]);
  const [generalModal, setGeneralModal] = useState(false);
  const [generalModalComponent, setGeneralModalComponent] = useState();

  /**
   * The default navigation function of Rigby, please limit yourself of using ONLY this function to navigate within screens, since this function keeps track of the navigation on the global app state.
   * @param {*} NavPropState The navigation prop passed to a screen from React-navigation i.e.: props.navigation
   * @param {*} screen the screen to navgate to.
   */
  function appNavigation(NavPropState, screen, screenOptions) {
    setNavigation(NavPropState);
    history.push({screen, screenOptions});
    setCurrentScreen(screenOptions ? screenOptions.screen : screen);
    console.log('Navigating with history');
    return NavPropState.navigate(screen, screenOptions);
  }
  function appNavigationWithoutHistory(NavPropState, screen, screenOptions) {
    setNavigation(NavPropState);
    setCurrentScreen(screenOptions ? screenOptions.screen : screen);
    console.log('Navigating without history');
    return NavPropState.navigate(screen, screenOptions);
  }
  /**
   * Navigates to the main flow
   * @param {*} NavPropState The navigation prop passed to a screen from React-navigation i.e.: props.navigation
   */
  function appGoBack(NavPropState) {
    setNavigation(NavPropState);
    let lastItem = history[history.length - 2];
    setCurrentScreen(
      lastItem.screenOptions ? lastItem.screenOptions.screen : lastItem.screen,
    );
    history.pop();
    //console.log(lastItem);
    return NavPropState.navigate(lastItem.screen, lastItem.screenOptions);
  }

  return (
    <GeneralConfigContext.Provider
      value={{
        theme,
        setTheme,
        loading,
        setLoading,
        copies,
        setCopies,
        locale,
        setLocale,
        navigation,
        setNavigation,
        currentScreen,
        setCurrentScreen,
        appNavigation,
        appNavigationWithoutHistory,
        appGoBack,
        generalModal,
        setGeneralModal,
        generalModalComponent,
        setGeneralModalComponent,
      }}>
      {props.children}
    </GeneralConfigContext.Provider>
  );
};
GeneralConfigProvider.propTypes = {
  ...model.types,
};

GeneralConfigProvider.defaultProps = {
  ...model.default,
};
