import {SlideRightView} from './Right/SlideRightView.component';
/**
 * A view that has a slide in animation
 * Contains 2 variants: Left and Right
 */
export const SlideInView = {
  Right: SlideRightView,
};
