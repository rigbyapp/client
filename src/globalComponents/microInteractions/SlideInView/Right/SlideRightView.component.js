import React, {useRef, useEffect} from 'react';
import {Animated, Easing} from 'react-native';
//Model
import {model} from './SlideRightView.model';
/**
 * A view that slides in from the right
 * @param {*} props
 */
export const SlideRightView = (props) => {
  const slideRight = useRef(new Animated.Value(props.distance)).current;
  useEffect(() => {
    Animated.timing(slideRight, {
      toValue: 0,
      duration: props.duration,
      useNativeDriver: true,
      delay: props.delay,
      easing: Easing.bezier(0.19, 1.0, 0.22, 1.0),
    }).start();
  }, [slideRight, props.duration, props.delay]);

  return (
    <Animated.View // Special animatable View
      style={{
        ...props.style,
        transform: [{translateX: slideRight}],
        // Bind opacity to animated value
      }}>
      {props.children}
    </Animated.View>
  );
};

SlideRightView.propTypes = {
  ...model.types,
};

SlideRightView.defaultProps = {
  ...model.default,
};
