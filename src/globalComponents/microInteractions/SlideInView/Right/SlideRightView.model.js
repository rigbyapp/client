/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    distance: PropTypes.number.isRequired,
    duration: PropTypes.number.isRequired,
    delay: PropTypes.number.isRequired,
  },
  default: {
    distance: 94,
    duration: 800,
    delay: 0,
  },
};
