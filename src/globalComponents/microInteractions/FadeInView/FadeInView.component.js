import React, {useRef, useEffect} from 'react';
import {Animated, Easing} from 'react-native';
//Model
import {model} from './FadeInView.model';
/**
 * A view that has a fade in animation
 * @param {*} props
 */
export const FadeInView = (props) => {
  const fadeAnim = useRef(new Animated.Value(0)).current; // Initial value for opacity: 0
  useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: props.duration,
      delay: props.delay,
      useNativeDriver: true,
      easing: Easing.in,
    }).start();
  }, [fadeAnim, props.duration, props.delay]);

  return (
    <Animated.View // Special animatable View
      style={{
        ...props.style,
        opacity: fadeAnim, // Bind opacity to animated value
      }}>
      {props.children}
    </Animated.View>
  );
};

FadeInView.propTypes = {
  ...model.types,
};

FadeInView.defaultProps = {
  ...model.default,
};
