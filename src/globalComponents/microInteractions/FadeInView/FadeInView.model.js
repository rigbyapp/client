/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    duration: PropTypes.number.isRequired,
    delay: PropTypes.number.isRequired,
  },
  default: {
    duration: 800,
    delay: 0,
  },
};
