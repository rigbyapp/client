import React, {useEffect, useRef} from 'react';
import {Animated, Easing} from 'react-native';
//Style
import {styles} from './Skeleton.style';
//Model
import {model} from './Skeleton.model';
/**
 * The base skeleton component
 * @param {*} props
 */
export const Skeleton = (props) => {
  var fadeAnim = useRef(new Animated.Value(1)).current; // Initial value for opacity: 0
  useEffect(() => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(fadeAnim, {
          toValue: 0.5,
          duration: 1000,
          useNativeDriver: true,
          easing: Easing.in,
        }),
        Animated.timing(fadeAnim, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: true,
          easing: Easing.in,
        }),
      ]),
    ).start();
  });
  return (
    <Animated.View
      style={{
        ...styles.base,
        opacity: fadeAnim,
        width: props.width,
        height: props.height,
        borderRadius: props.borderRadius,
        ...props.style,
      }}
    />
  );
};
Skeleton.propTypes = {
  ...model.types,
};

Skeleton.defaultProps = {
  ...model.default,
};
