import {StyleSheet} from 'react-native';
import {colors} from '@rigby/theme/colors.style';
export const styles = StyleSheet.create({
  base: {
    backgroundColor: colors.inputBackgroundFocused,
  },
});
