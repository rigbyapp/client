/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    borderRadius: PropTypes.number.isRequired,
  },
  default: {
    width: 50,
    height: 50,
    borderRadius: 0,
  },
};
