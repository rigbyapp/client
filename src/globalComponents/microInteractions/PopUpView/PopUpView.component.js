import React, {useRef, useEffect} from 'react';
import {Animated, Easing} from 'react-native';
//Model
import {model} from './PopUpView.model';
/**
 * A view with a pop up animation
 * @param {*} props
 */
export const PopUpView = (props) => {
  const popUpAnim = useRef(new Animated.Value(props.distance)).current; // Initial value for opacity: 0

  useEffect(() => {
    Animated.timing(popUpAnim, {
      toValue: 0,
      duration: props.duration,
      delay: props.delay,
      useNativeDriver: true,
      easing: Easing.bezier(0.19, 1.0, 0.22, 1.0),
    }).start();
  }, [popUpAnim, props.duration, props.delay]);

  return (
    <Animated.View // Special animatable View
      style={{
        ...props.style,
        transform: [{translateY: popUpAnim}],
        // Bind opacity to animated value
      }}>
      {props.children}
    </Animated.View>
  );
};

PopUpView.propTypes = {
  ...model.types,
};

PopUpView.defaultProps = {
  ...model.default,
};
