import {StyleSheet} from 'react-native';
import {layout} from '@rigby/theme/layout.style';
import {colors} from '@rigby/theme/colors.style';

export const styles = StyleSheet.create({
  loadingBar: {
    backgroundColor: colors.background,
    width: '100%',
    height: 10,
    zIndex: 6,
  },
  innerWrapper: {
    flexDirection: 'row',
    width: '20%',
  },
  innerBar: {
    width: '100%',
    height: 10,
  },
});
