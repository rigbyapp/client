import React, {useRef, useEffect} from 'react';
import {View, Animated, Dimensions, Easing} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
//Style
import {styles} from './LoadingBar.style';
//Rigby
import {gradients} from '@rigby/theme/colors.style';
/**
 * The loading bar used on the header
 */
export const LoadingBar = () => {
  const dim = Dimensions.get('window');
  const slideRight = useRef(new Animated.Value(dim.width * -0.4)).current; // Initial value for opacity: 0
  useEffect(() => {
    Animated.loop(
      Animated.timing(slideRight, {
        toValue: dim.width,
        duration: 700,
        useNativeDriver: true,
        easing: Easing.bezier(0.5, 0, 1, 1),
      }),
    ).start();
  }, [slideRight, dim]);
  return (
    <View style={styles.loadingBar}>
      <Animated.View
        style={{...styles.innerWrapper, transform: [{translateX: slideRight}]}}>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={gradients.gradient3}
          style={styles.innerBar}
        />
        <LinearGradient
          start={{x: 1, y: 0}}
          end={{x: 0, y: 0}}
          colors={gradients.gradient3}
          style={styles.innerBar}
        />
      </Animated.View>
    </View>
  );
};
