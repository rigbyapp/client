import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  textWrapper: {
    paddingVertical: 30,
    width: '100%',
  },
});
