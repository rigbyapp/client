import React from 'react';
//Style
import {styles} from './Notice.style';
//Model
import {model} from './Notice.model';
//Rigby
import {Typography} from '@rigby/components/text/Typography';
import {PopUpView} from '@rigby/components/microInteractions/PopUpView';

/**
 * A component for displaying notices with an image; commonly used for things like no post found, no internet connection and such
 * @param {*} props
 */
export const Notice = (props) => {
  return (
    <PopUpView style={styles.textWrapper}>
      <Typography align={'center'}>{props.message}</Typography>
    </PopUpView>
  );
};
Notice.propTypes = {
  ...model.types,
};

Notice.defaultProps = {
  ...model.default,
};
