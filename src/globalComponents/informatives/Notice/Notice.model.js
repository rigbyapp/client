/********** Model of this component ************/
import PropTypes from 'prop-types';

export const model = {
  types: {
    message: PropTypes.string.isRequired,
  },
  default: {
    message: 'NOTICE_EXAMPLE',
  },
};
