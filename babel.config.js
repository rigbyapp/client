module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['.'],
        extensions: [
          '.ios.ts',
          '.android.ts',
          '.ts',
          '.ios.tsx',
          '.android.tsx',
          '.tsx',
          '.jsx',
          '.js',
          '.json',
        ],
        alias: {
          '@rigby/assets': './src/assets',
          '@rigby/utils': './src/utils',
          '@rigby/components': './src/globalComponents',
          '@rigby/theme': './src/utils/theme',
          '@rigby/commons': './src/utils/commons',
          '@rigby/hooks': './src/utils/commons/hooks',
          '@rigby/data': './src/utils/data',
          '@rigby/services': './src/utils/commons/services',
          '@rigby/modules': './src/modules',
          '@rigby/context': './src/context',
          '@rigby/config': './env.js',
        },
      },
    ],
  ],
};
