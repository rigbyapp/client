const envs = {
  production: {
    HOST_BACKEND: 'https://rigbycoreservices.uc.r.appspot.com/',
    PUBLIC_BUCKET: 'https://storage.googleapis.com/ry_bucket_media_public/',
  },

  development: {
    HOST_BACKEND: 'http://localhost:8080/',
    PUBLIC_BUCKET: 'https://storage.googleapis.com/ry_bucket_media_public/',
  },
};

export const ENV_CONFIG = envs['development'];
