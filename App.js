/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import {KeyboardAvoidingView, StatusBar} from 'react-native';
import {AppRouter} from '@rigby/modules/router';
import {GeneralConfigProvider, UserProvider} from '@rigby/components/context';
import {Header} from '@rigby/components/navigation/Header';
import {GeneralAppModal} from '@rigby/components/layout/GeneralAppModal';
import {iconLibrary} from '@rigby/theme/iconLibrary/iconLibrary';
import {colors} from '@rigby/theme/colors.style';

//Import icon Library
iconLibrary();

const App: () => React$Node = () => {
  return (
    <GeneralConfigProvider>
      <UserProvider>
        <Header />
        <StatusBar barStyle="light-content" />
        <KeyboardAvoidingView
          style={{flex: 1, flexDirection: 'column', justifyContent: 'center'}}
          behavior="height"
          enabled>
          <AppRouter />
          <GeneralAppModal />
        </KeyboardAvoidingView>
      </UserProvider>
    </GeneralConfigProvider>
  );
};

export default App;
